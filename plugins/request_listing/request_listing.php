<?php
/*
Plugin Name: Request Listings
Description: Request Listings
Version: 1.0.0
Plugin URI: https://www.fiverr.com/wp_right  
Author: LogicsBuffer
Author URI: http://logicsbuffer.com/
*/

global $curent_fav;
global $currency;
global $where_currency;
global $show_compare;
global $show_compare_only;
global $show_remove_fav;
global $options;
global $isdashabord;
global $align;
global $align_class;
global $is_shortcode;
global $row_number_col;
global $is_col_md_12;
global $prop_unit;
global $property_unit_slider;
global $custom_unit_structure;
global $no_listins_per_row;
global $wpestate_uset_unit;

$pinterest          =   '';
$previe             =   '';
$compare            =   '';
$extra              =   '';
$property_size      =   '';
$property_bathrooms =   '';
$property_rooms     =   '';
$measure_sys        =   '';



if($no_listins_per_row==3){
    $col_class  =   'col-md-6';
    $col_org    =   6;
}else{   
    $col_class  =   'col-md-4';
    $col_org    =   4;
}


if($options['content_class']=='col-md-12' && $show_remove_fav!=1){
    if($no_listins_per_row==3){
        $col_class  =   'col-md-4';
        $col_org    =   4;
    }else{
        $col_class  =   'col-md-3';
        $col_org    =   3;
    }
    
}
// if template is vertical
if($align=='col-md-12'){
    $col_class  =  'col-md-12';
    $col_org    =  12;
}



if(isset($is_shortcode) && $is_shortcode==1 ){
    $col_class='col-md-'.$row_number_col.' shortcode-col';
}

if(isset($is_col_md_12) && $is_col_md_12==1){
    $col_class  =   'col-md-6';
    $col_org    =   6;
}


if($curent_fav){
    if ( in_array ($post->ID,$curent_fav) ){
    $favorite_class =   'icon-fav-on';   
    $fav_mes        =   __('remove from favorites','wpestate');
    } 
}
if(isset($prop_unit) && $prop_unit=='list'){
   $col_class= 'col-md-12';
}

if( $property_unit_slider==1){
    $col_class.=' has_prop_slider ';
}

if($no_listins_per_row==4){
    $col_class.=' has_4per_row ';
}
$is_custom_desing=11;
  
add_action('wp_enqueue_scripts', 'request_listing_script_front_css');
add_action('wp_enqueue_scripts', 'request_listing_script_front_js');
add_action('admin_enqueue_scripts', 'request_listing_script_back_css');
add_action('init', 'wp_astro_init');
function wp_astro_init() {
   add_shortcode('show_request_listing_form', 'wp_request_listing_form');
   add_shortcode('show_paid_listing', 'rl_show_paid_listing');
   add_shortcode('show_post_property_form', 'wp_property_form');
   add_shortcode('show_admin_offers', 'wp_admin_offers');
}


  //Theme Functions
  if( !function_exists('wpestate_strip_excerpt_by_char') ):
    function wpestate_strip_excerpt_by_char($text, $chars_no,$post_id) {
        $return_string  = '';
        $return_string  =  mb_substr( $text,0,$chars_no); 
            if(mb_strlen($text)>$chars_no){
                $return_string.= ' <a href="'.get_permalink($post_id).'" class="unit_more_x">'.__('[more]','wpestate').'</a>';   
            } 
        return $return_string;
        }
        
  endif; // end   wpestate_strip_words 

  if( !function_exists('wpestate_strip_excerpt_by_char_places') ):
      function wpestate_strip_excerpt_by_char_places($text, $chars_no,$link) {
          $return_string  = '';
          $return_string  =  mb_substr( $text,0,$chars_no); 
              if(mb_strlen($text)>$chars_no){
                  $return_string.= ' <a href="'.$link.'" class="unit_more_x">'.__('[more]','wpestate').'</a>';   
              } 
          return $return_string;
          }
          
  endif; // end   wpestate_strip_words 

if( !function_exists('wpestate_show_price_label_slider') ):
function wpestate_show_price_label_slider($min_price_slider,$max_price_slider,$currency,$where_currency){

    $th_separator       =  stripslashes(  get_option('wp_estate_prices_th_separator','') );
    
        
    $custom_fields = get_option( 'wp_estate_multi_curr', true);
    //print_r($_COOKIE);
    if( !empty($custom_fields) && isset($_COOKIE['my_custom_curr']) &&  isset($_COOKIE['my_custom_curr_pos']) &&  isset($_COOKIE['my_custom_curr_symbol']) && $_COOKIE['my_custom_curr_pos']!=-1){
        $i=intval($_COOKIE['my_custom_curr_pos']);
        
        if( !isset($_GET['price_low']) && !isset($_GET['price_max'])  ){
            $min_price_slider       =   $min_price_slider * $custom_fields[$i][2];
            $max_price_slider       =   $max_price_slider * $custom_fields[$i][2];
        }
        
        $currency               =   $custom_fields[$i][0];
        $min_price_slider   =   number_format($min_price_slider,0,'.',$th_separator);
        $max_price_slider   =   number_format($max_price_slider,0,'.',$th_separator);
        
        if ($custom_fields[$i][3] == 'before') {  
            $price_slider_label = $currency .' '. $min_price_slider.' '.__('to','wpestate').' '.$currency .' '. $max_price_slider;      
        } else {
            $price_slider_label =  $min_price_slider.' '.$currency.' '.__('to','wpestate').' '.$max_price_slider.' '.$currency;      
        }
        
    }else{
        $min_price_slider   =   number_format($min_price_slider,0,'.',$th_separator);
        $max_price_slider   =   number_format($max_price_slider,0,'.',$th_separator);
        
        if ($where_currency == 'before') {
            $price_slider_label = $currency .' '.($min_price_slider).' '.__('to','wpestate').' '.$currency .' ' .$max_price_slider;
        } else {
            $price_slider_label =  $min_price_slider.' '.$currency.' '.__('to','wpestate').' '.$max_price_slider.' '.$currency;
        }  
    }
    
    return $price_slider_label;
                            
    
}
endif;



/////////////////////////////////////////////////////////////////////////////////
// show price
///////////////////////////////////////////////////////////////////////////////////


if( !function_exists('wpestate_show_price') ):
function wpestate_show_price($post_id,$currency,$where_currency,$return=0){
      
    $price_label        = '<span class="price_label">'.esc_html ( get_post_meta($post_id, 'property_label', true) ).'</span>';
    $price_label_before = '<span class="price_label price_label_before">'.esc_html ( get_post_meta($post_id, 'property_label_before', true) ).'</span>';
    $price              = floatval( get_post_meta($post_id, 'property_price', true) );
    
    $th_separator   = stripslashes ( get_option('wp_estate_prices_th_separator','') );
    $custom_fields  = get_option( 'wp_estate_multi_curr', true);
    //print_r($_COOKIE);
    if( !empty($custom_fields) && isset($_COOKIE['my_custom_curr']) &&  isset($_COOKIE['my_custom_curr_pos']) &&  isset($_COOKIE['my_custom_curr_symbol']) && $_COOKIE['my_custom_curr_pos']!=-1){
        $i=intval($_COOKIE['my_custom_curr_pos']);
        $custom_fields = get_option( 'wp_estate_multi_curr', true);
        if ($price != 0) {
            $price      = $price * $custom_fields[$i][2];
            $price      = number_format($price,0,'.',$th_separator);
           
            $currency   = $custom_fields[$i][0];
            
            if ($custom_fields[$i][3] == 'before') {
                $price = $currency . ' ' . $price;
            } else {
                $price = $price . ' ' . $currency;
            }
            
        }else{
            $price='';
        }
    }else{
        if ($price != 0) {
           $price = number_format($price,0,'.',$th_separator);

            if ($where_currency == 'before') {
                $price = $currency . ' ' . $price;
            } else {
                $price = $price . ' ' . $currency;
            }
            
        }else{
            $price='';
        }
    }
    
  
    
    if($return==0){
        print $price_label_before.' '.$price.' '.$price_label;
    }else{
        return $price_label_before.' '.$price.' '.$price_label;
    }
}
endif;


/////////////////////////////////////////////////////////////////////////////////
// show price
///////////////////////////////////////////////////////////////////////////////////


if( !function_exists('wpestate_show_price_floor') ):
function wpestate_show_price_floor($price,$currency,$where_currency,$return=0){
      

    
    $th_separator   = stripslashes ( get_option('wp_estate_prices_th_separator','') );
    $custom_fields  = get_option( 'wp_estate_multi_curr', true);

    //print_r($_COOKIE);
    if( !empty($custom_fields) && isset($_COOKIE['my_custom_curr']) &&  isset($_COOKIE['my_custom_curr_pos']) &&  isset($_COOKIE['my_custom_curr_symbol']) && $_COOKIE['my_custom_curr_pos']!=-1){
        $i=intval($_COOKIE['my_custom_curr_pos']);
        $custom_fields = get_option( 'wp_estate_multi_curr', true);
        if ($price != 0) {
            
                $price      = $price * $custom_fields[$i][2];
           
           
            $price      = number_format($price,0,'.',$th_separator);
           
            $currency   = $custom_fields[$i][0];
            
            if ($custom_fields[$i][3] == 'before') {
                $price = $currency . ' ' . $price;
            } else {
                $price = $price . ' ' . $currency;
            }
            
        }else{
            $price='';
        }
    }else{
        if ($price != 0) {
        
           $price = number_format($price,0,'.',$th_separator);

            if ($where_currency == 'before') {
                $price = $currency . ' ' . $price;
            } else {
                $price = $price . ' ' . $currency;
            }
            
        }else{
            $price='';
        }
    }
    
  
    
    if($return==0){
        print $price;
    }else{
        return $price;
    }
}
endif;


if( !function_exists('wpestate_virtual_tour_details') ):
function wpestate_virtual_tour_details($post_id) {
    echo $virtual_tour                   =   get_post_meta($post_id, 'embed_virtual_tour', true);
      
}
endif;

  //Theme Functions Ends


function wp_admin_offers( $atts ) {
if(isset($_POST['approve_offer'])){
$buyer_emial = $_POST['buyer_emial'];
$subject = 'The DDDsubject';
$body = 'The DDDD email body content';
$headers = array('Content-Type: text/html; charset=UTF-8');
 
wp_mail( $buyer_emial, $subject, $body, $headers );
  }
  $user_id = get_current_user_id();
  $admin_location_id ='user_'.$user_id;
  $user_data = get_userdata( $user_id );
  $user_role = $user_data->roles[0];
  $admin_location = get_field('admin_location',$admin_location_id);
if($user_role == 'area_admin' || $user_role == 'administrator'){
    // define attributes and their defaults
    extract( shortcode_atts( array (
        'type' => 'property_offer',
        'order' => 'date',
        'orderby' => 'title',
        'offer_city' => $admin_location,
        'posts_per_page' => -1,
    ), $atts ) );
 
    // define query parameters based on attributes
    $options = array(
    'post_type' => $type,
        'offer_city' => $offer_city,
        'posts_per_page' => $posts_per_page,
        'order' => 'ASC',
        'orderby' => 'title',
    );
    $query = new WP_Query( $options );
    // run the loop based on the query
  ob_start();

    if ( $query->have_posts() ) { ?>
	 <div class="names-listing">
            <?php while ( $query->have_posts() ) : $query->the_post(); 
      $id = get_the_ID();
      $buyer_emial = get_field('buyer_emial',$id);
      ///$baby_category = get_the_terms( $post->ID,'baby_gender'); ?>
      
	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
	</div>
	<div>
		<form class="" action="" method="post" name="" id="">
		   <input type="text" name="buyer_emial" value="<?php echo $buyer_emial; ?>">
			 <div class="row">
			   <div class="">
			   <?php edit_post_link('edit Offer', '<p>', '</p>',$id); ?>
				  <button id="input_2" name="approve_offer" type="submit" class="wpresidence_button" data-component="button">
				  Approve
				  </button>
			   </div>
			 </div>
		</form> 
	 </div>
	<?php endwhile; ?>
	</div>
			<?php
            wp_reset_postdata(); ?>
        

    <?php
        $myvariable = ob_get_clean();
        return $myvariable;
    }
  }
}
function wp_property_form($atts) {
  if(isset($_POST['submit_property'])){
  $formID = $_POST['formID'];
    $input_language = $_POST['input_language'];
    $smsConfirmation = $_POST['smsConfirmation'];
  }
  ob_start();
  ?>
<form id="new_post" name="new_post" method="post" action="" enctype="multipart/form-data" class="add-estate">
   <div class="col-md-12 row_dasboard-prop-listing">
   </div>
   <div class="profile-page row">
      <div class="col-md-9 user_dashboard">
         <div class="col-md-12 add-estate profile-page profile-onprofile row">
            <div class="submit_container">
               <div class="col-md-4 profile_label">
                  <!--<div class="submit_container_header">Property Request Description & Price</div>-->
                  <div class="user_details_row">Property Request Description</div>
                  <div class="user_profile_explain">This description will appear first in page. Keeping it as a brief overview makes it easier to read.</div>
               </div>
               <div class="col-md-8">
                  <p class="full_form">
                     <label for="title">*I am (mandatory) </label>
                     <select class="form-dropdown validate[required]" id="input_18" name="q18_iAm" style="width:150px" data-component="dropdown" required="">
                        <option value="">  </option>
                        <option selected="" value="A Seller"> A Seller </option>
                        <option value="A Renter"> A Renter </option>
                        <option value="Representing the property owner(s)"> Representing the property owner(s) </option>
                     </select>
                  </p>
                  <div class="form-line" data-type="control_number" id="id_50">
                     <label class="form-label form-label-top form-label-auto" id="label_50" for="input_50"> If you are a real estate broker please provide a license number </label>
                     <div id="cid_50" class="form-input-wide">
                        <input type="number" id="input_50" name="q50_ifYou50" data-type="input-number" class=" form-number-input form-textbox" style="width:116px" size="12" value="" placeholder="ex: 23" data-component="number">
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-12 add-estate profile-page profile-onprofile row">
            <div class="submit_container">
               <div class="col-md-4 profile_label">
                  <!--<div class="submit_container_header">Property Request Description & Price</div>-->
                  <div class="user_details_row">Are you a Real Estate agent?</div>
               </div>
               <div class="col-md-8">
                  <div class="form-line jf-required" data-type="control_radio" id="id_46">
                     <div id="cid_46" class="form-input-wide jf-required">
                        <div class="form-single-column" data-component="radio">
                           <span class="form-radio-item" style="clear:left">
                           <span class="dragger-item">
                           </span>
                           <input type="radio" class="form-radio validate[required]" id="input_46_0" name="q46_areYou" value="No" required="">
                           <label id="label_input_46_0" for="input_46_0"> No </label>
                           </span>
                           <span class="form-radio-item" style="clear:left">
                           <span class="dragger-item">
                           </span>
                           <input type="radio" class="form-radio validate[required]" id="input_46_1" name="q46_areYou" value="Yes, not charging any commission from the buyer." required="">
                           <label id="label_input_46_1" for="input_46_1"> Yes, not charging any commission from the buyer. </label>
                           </span>
                           <span class="form-radio-item" style="clear:left">
                           <span class="dragger-item">
                           </span>
                           <input type="radio" class="form-radio validate[required]" id="input_46_2" name="q46_areYou" value="Yes, charging commission from the buyer." required="">
                           <label id="label_input_46_2" for="input_46_2"> Yes, charging commission from the buyer. </label>
                           </span>
                        </div>
                     </div>
                  </div>
                  <p class="col-md-6 half_form half_form_last">
                     <label for="property_label">After Price Label (ex: "/month"</label>
                     <input type="text" id="property_label" class="form-control" size="40" name="property_label" value="">
                  </p>
                  <p class="col-md-6 half_form">
                     <label for="property_label_before">Before Price Label (ex: "from ")</label>
                     <input type="text" id="property_label_before" class="form-control" size="40" name="property_label_before" value="">
                  </p>
               </div>
            </div>
         </div>
		  <div class="col-md-12 add-estate profile-page profile-onprofile row">
            <div class="submit_container">
                <p class="col-md-6"><label for="full_name">Full Name  (*text) </label><input type="text" id="full_name" size="40" name="full_name" value=""></p>
				<p class="col-md-6"><label for="email">Emial (*text) </label><input type="text" id="email" size="40" name="email" value=""></p>
				<p class="col-md-6"><label for="mobile">Mobile (*number) </label><input type="text" id="mobile" size="40" name="mobile" value=""></p>
            </div>
         </div>
		
         <div class="col-md-12 add-estate profile-page profile-onprofile row">
            <div class="submit_container">
               <div class="col-md-4 profile_label">
                  <!--<div class="submit_container_header">Property type</div>-->
                  <div class="user_details_row">Property type</div>
                  <div class="user_profile_explain">Selecting a Property type will make it easier for users to find you property in search results.</div>
               </div>
               <p class="col-md-4">
                  <label for="prop_category">Property type</label>
                  <select class="form-dropdown validate[required] form-validation-error" id="input_20" name="q20_propertyType" style="width:150px" data-component="dropdown" required="">
                     <option value="">  </option>
                     <option value="Apartment"> Apartment </option>
                     <option value="House"> House </option>
                     <option value="Penthouse"> Penthouse </option>
                     <option value="Duplex"> Duplex </option>
                     <option value="Studio"> Studio </option>
                     <option value="Garden Apartment"> Garden Apartment </option>
                     <option value="Unit"> Unit </option>
                     <option value="Top Apartment"> Top Apartment </option>
                     <option value="Loft"> Loft </option>
                     <option value="Studio"> Studio </option>
                     <option value="Vacation Apartment"> Vacation Apartment </option>
                     <option value="Basement"> Basement </option>
                     <option value="B &amp; B"> B &amp; B </option>
                     <option value="Farm"> Farm </option>
                     <option value="Storage"> Storage </option>
                     <option value="Parking Spot"> Parking Spot </option>
                     <option value="Nursing Home"> Nursing Home </option>
                     <option value="Plots"> Plots </option>
                     <option value="Building"> Building </option>
                     <option value="Various Asset"> Various Asset </option>
                  </select>
               </p>
            </div>
         </div>
		 <div class="col-md-12 add-estate profile-page profile-onprofile row">
            <div class="submit_container ">
               <div class="col-md-4 profile_label">
                  <!--<div class="submit_container_header">Amenities and Features</div>-->
                  <div class="user_details_row">Additions to the property *</div>
               </div>
               <div class="col-md-8">
                  <div class="form-single-column" data-component="checkbox">
                     <span class="form-checkbox-item" style="clear:left">
                     <span class="dragger-item">
                     </span>
                     <input type="checkbox" class="form-checkbox validate[required]" id="input_37_0" name="q37_proportyFeatures[]" value="There are no additions" required="">
                     <label id="label_input_37_0" for="input_37_0"> There are no additions </label>
                     </span>
                     <span class="form-checkbox-item" style="clear:left">
                     <span class="dragger-item">
                     </span>
                     <input type="checkbox" class="form-checkbox validate[required]" id="input_37_1" name="q37_proportyFeatures[]" value="Parking Spot" required="">
                     <label id="label_input_37_1" for="input_37_1"> Parking Spot </label>
                     </span>
                     <span class="form-checkbox-item" style="clear:left">
                     <span class="dragger-item">
                     </span>
                     <input type="checkbox" class="form-checkbox validate[required]" id="input_37_2" name="q37_proportyFeatures[]" value="Elevator" required="">
                     <label id="label_input_37_2" for="input_37_2"> Elevator </label>
                     </span>
                     <span class="form-checkbox-item" style="clear:left">
                     <span class="dragger-item">
                     </span>
                     <input type="checkbox" class="form-checkbox validate[required]" id="input_37_3" name="q37_proportyFeatures[]" value="Air Condition" required="">
                     <label id="label_input_37_3" for="input_37_3"> Air Condition </label>
                     </span>
                     <span class="form-checkbox-item" style="clear:left">
                     <span class="dragger-item">
                     </span>
                     <input type="checkbox" class="form-checkbox validate[required]" id="input_37_4" name="q37_proportyFeatures[]" value="Shelter" required="">
                     <label id="label_input_37_4" for="input_37_4"> Shelter </label>
                     </span>
                     <span class="form-checkbox-item" style="clear:left">
                     <span class="dragger-item">
                     </span>
                     <input type="checkbox" class="form-checkbox validate[required]" id="input_37_5" name="q37_proportyFeatures[]" value="Renovated" required="">
                     <label id="label_input_37_5" for="input_37_5"> Renovated </label>
                     </span>
                     <span class="form-checkbox-item" style="clear:left">
                     <span class="dragger-item">
                     </span>
                     <input type="checkbox" class="form-checkbox validate[required]" id="input_37_6" name="q37_proportyFeatures[]" value="Balcony" required="">
                     <label id="label_input_37_6" for="input_37_6"> Balcony </label>
                     </span>
                     <span class="form-checkbox-item" style="clear:left">
                     <span class="dragger-item">
                     </span>
                     <input type="checkbox" class="form-checkbox validate[required]" id="input_37_7" name="q37_proportyFeatures[]" value="Sun terrace" required="">
                     <label id="label_input_37_7" for="input_37_7"> Sun terrace </label>
                     </span>
                     <span class="form-checkbox-item" style="clear:left">
                     <span class="dragger-item">
                     </span>
                     <input type="checkbox" class="form-checkbox validate[required]" id="input_37_8" name="q37_proportyFeatures[]" value="Storage Room" required="">
                     <label id="label_input_37_8" for="input_37_8"> Storage Room </label>
                     </span>
                     <span class="form-checkbox-item" style="clear:left">
                     <span class="dragger-item">
                     </span>
                     <input type="checkbox" class="form-checkbox validate[required]" id="input_37_9" name="q37_proportyFeatures[]" value="Disabled Access" required="">
                     <label id="label_input_37_9" for="input_37_9"> Disabled Access </label>
                     </span>
                  </div>
               </div>
            </div>
         </div>
         
         <div class="col-md-12 add-estate profile-page profile-onprofile row advertise_us_page">
            <div class="submit_container">
               <div class="col-md-12 profile_label advertise_us_page">
                  <!--<div class="submit_container_header">Listing Location</div>-->
                  <div class="user_details_row">Listing Location</div>
                  <div class="user_profile_explain">Use the button to save your property location on the map as well.</div>
               </div>
               <div class="col-md-12 advertise_us_page">
                  <p class="full_form">
                     <label for="property_address">*Address (mandatory) </label>
                     <input type="text" placeholder="Enter address" id="property_address" class="form-control" size="40" name="property_address" rows="3" cols="42" value="" autocomplete="off">
                  </p>
                  <div class="advanced_city_div half_form">
                     <label for="property_city">City</label>
                     <input type="text" id="property_city_submit" name="property_city" class="form-control" placeholder="Enter city" size="40" value="" autocomplete="off">
                  </div>
                  <div class="advanced_area_div half_form half_form_last">
                     <label for="property_area">Neighborhood</label>
                     <input type="text" id="property_area" name="property_area" class="form-control" size="40" value="">
                     <!--
                        -->
                  </div>
                  <p class="half_form">
                     <label for="property_zip">Zip </label>
                     <input type="text" id="property_zip" class="form-control" size="40" name="property_zip" value="">
                  </p>
                  <!--
                     <p class="half_form ">
                         <label for="property_state">State </label>
                         <input type="text" id="property_state" class="form-control" size="40" name="property_state" value="">
                     </p>-->
                  <p class="half_form  half_form_last">
                     <label for="property_county">County / State</label>
                     <input type="text" id="property_county" class="form-control" size="40" name="property_county" value="">
                  </p>
                  <p class="half_form ">
                     <label for="property_country">Country </label>
                     <select id="property_country" name="property_country" class="select-submit2">
                        <option value="Afghanistan">Afghanistan</option>
                        <option value="Albania">Albania</option>
                        <option value="Algeria">Algeria</option>
                        <option value="American Samoa">American Samoa</option>
                        <option value="Andorra">Andorra</option>
                        <option value="Angola">Angola</option>
                        <option value="Anguilla">Anguilla</option>
                        <option value="Antarctica">Antarctica</option>
                        <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                        <option value="Argentina">Argentina</option>
                        <option value="Armenia">Armenia</option>
                        <option value="Aruba">Aruba</option>
                        <option value="Australia">Australia</option>
                        <option value="Austria">Austria</option>
                        <option value="Azerbaijan">Azerbaijan</option>
                        <option value="Bahamas">Bahamas</option>
                        <option value="Bahrain">Bahrain</option>
                        <option value="Bangladesh">Bangladesh</option>
                        <option value="Barbados">Barbados</option>
                        <option value="Belarus">Belarus</option>
                        <option value="Belgium">Belgium</option>
                        <option value="Belize">Belize</option>
                        <option value="Benin">Benin</option>
                        <option value="Bermuda">Bermuda</option>
                        <option value="Bhutan">Bhutan</option>
                        <option value="Bolivia">Bolivia</option>
                        <option value="Bosnia and Herzegowina">Bosnia and Herzegowina</option>
                        <option value="Botswana">Botswana</option>
                        <option value="Bouvet Island">Bouvet Island</option>
                        <option value="Brazil">Brazil</option>
                        <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                        <option value="Brunei Darussalam">Brunei Darussalam</option>
                        <option value="Bulgaria">Bulgaria</option>
                        <option value="Burkina Faso">Burkina Faso</option>
                        <option value="Burundi">Burundi</option>
                        <option value="Cambodia">Cambodia</option>
                        <option value="Cameroon">Cameroon</option>
                        <option value="Canada">Canada</option>
                        <option value="Cape Verde">Cape Verde</option>
                        <option value="Cayman Islands">Cayman Islands</option>
                        <option value="Central African Republic">Central African Republic</option>
                        <option value="Chad">Chad</option>
                        <option value="Chile">Chile</option>
                        <option value="China">China</option>
                        <option value="Christmas Island">Christmas Island</option>
                        <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                        <option value="Colombia">Colombia</option>
                        <option value="Comoros">Comoros</option>
                        <option value="Congo">Congo</option>
                        <option value="Congo, the Democratic Republic of the">Congo, the Democratic Republic of the</option>
                        <option value="Cook Islands">Cook Islands</option>
                        <option value="Costa Rica">Costa Rica</option>
                        <option value="Cote d'Ivoire">Cote d'Ivoire</option>
                        <option value="Croatia (Hrvatska)">Croatia (Hrvatska)</option>
                        <option value="Cuba">Cuba</option>
                        <option value="Cyprus">Cyprus</option>
                        <option value="Czech Republic">Czech Republic</option>
                        <option value="Denmark">Denmark</option>
                        <option value="Djibouti">Djibouti</option>
                        <option value="Dominica">Dominica</option>
                        <option value="Dominican Republic">Dominican Republic</option>
                        <option value="East Timor">East Timor</option>
                        <option value="Ecuador">Ecuador</option>
                        <option value="Egypt">Egypt</option>
                        <option value="El Salvador">El Salvador</option>
                        <option value="Equatorial Guinea">Equatorial Guinea</option>
                        <option value="Eritrea">Eritrea</option>
                        <option value="Estonia">Estonia</option>
                        <option value="Ethiopia">Ethiopia</option>
                        <option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
                        <option value="Faroe Islands">Faroe Islands</option>
                        <option value="Fiji">Fiji</option>
                        <option value="Finland">Finland</option>
                        <option value="France">France</option>
                        <option value="France Metropolitan">France Metropolitan</option>
                        <option value="French Guiana">French Guiana</option>
                        <option value="French Polynesia">French Polynesia</option>
                        <option value="French Southern Territories">French Southern Territories</option>
                        <option value="Gabon">Gabon</option>
                        <option value="Gambia">Gambia</option>
                        <option value="Georgia">Georgia</option>
                        <option value="Germany">Germany</option>
                        <option value="Ghana">Ghana</option>
                        <option value="Gibraltar">Gibraltar</option>
                        <option value="Greece">Greece</option>
                        <option value="Greenland">Greenland</option>
                        <option value="Grenada">Grenada</option>
                        <option value="Guadeloupe">Guadeloupe</option>
                        <option value="Guam">Guam</option>
                        <option value="Guatemala">Guatemala</option>
                        <option value="Guinea">Guinea</option>
                        <option value="Guinea-Bissau">Guinea-Bissau</option>
                        <option value="Guyana">Guyana</option>
                        <option value="Haiti">Haiti</option>
                        <option value="Heard and Mc Donald Islands">Heard and Mc Donald Islands</option>
                        <option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option>
                        <option value="Honduras">Honduras</option>
                        <option value="Hong Kong">Hong Kong</option>
                        <option value="Hungary">Hungary</option>
                        <option value="Iceland">Iceland</option>
                        <option value="India">India</option>
                        <option value="Indonesia">Indonesia</option>
                        <option value="Iran (Islamic Republic of)">Iran (Islamic Republic of)</option>
                        <option value="Iraq">Iraq</option>
                        <option value="Ireland">Ireland</option>
                        <option value="Israel">Israel</option>
                        <option value="Italy">Italy</option>
                        <option value="Jamaica">Jamaica</option>
                        <option value="Japan">Japan</option>
                        <option value="Jordan">Jordan</option>
                        <option value="Kazakhstan">Kazakhstan</option>
                        <option value="Kenya">Kenya</option>
                        <option value="Kiribati">Kiribati</option>
                        <option value="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of</option>
                        <option value="Korea, Republic of">Korea, Republic of</option>
                        <option value="Kuwait">Kuwait</option>
                        <option value="Kyrgyzstan">Kyrgyzstan</option>
                        <option value="Lao, People's Democratic Republic">Lao, People's Democratic Republic</option>
                        <option value="Latvia">Latvia</option>
                        <option value="Lebanon">Lebanon</option>
                        <option value="Lesotho">Lesotho</option>
                        <option value="Liberia">Liberia</option>
                        <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                        <option value="Liechtenstein">Liechtenstein</option>
                        <option value="Lithuania">Lithuania</option>
                        <option value="Luxembourg">Luxembourg</option>
                        <option value="Macau">Macau</option>
                        <option value="Macedonia (FYROM)">Macedonia (FYROM)</option>
                        <option value="Madagascar">Madagascar</option>
                        <option value="Malawi">Malawi</option>
                        <option value="Malaysia">Malaysia</option>
                        <option value="Maldives">Maldives</option>
                        <option value="Mali">Mali</option>
                        <option value="Malta">Malta</option>
                        <option value="Marshall Islands">Marshall Islands</option>
                        <option value="Martinique">Martinique</option>
                        <option value="Mauritania">Mauritania</option>
                        <option value="Mauritius">Mauritius</option>
                        <option value="Mayotte">Mayotte</option>
                        <option value="Mexico">Mexico</option>
                        <option value="Micronesia, Federated States of">Micronesia, Federated States of</option>
                        <option value="Moldova, Republic of">Moldova, Republic of</option>
                        <option value="Monaco">Monaco</option>
                        <option value="Mongolia">Mongolia</option>
                        <option value="Montserrat">Montserrat</option>
                        <option value="Morocco">Morocco</option>
                        <option value="Mozambique">Mozambique</option>
                        <option value="Montenegro">Montenegro</option>
                        <option value="Myanmar">Myanmar</option>
                        <option value="Namibia">Namibia</option>
                        <option value="Nauru">Nauru</option>
                        <option value="Nepal">Nepal</option>
                        <option value="Netherlands">Netherlands</option>
                        <option value="Netherlands Antilles">Netherlands Antilles</option>
                        <option value="New Caledonia">New Caledonia</option>
                        <option value="New Zealand">New Zealand</option>
                        <option value="Nicaragua">Nicaragua</option>
                        <option value="Niger">Niger</option>
                        <option value="Nigeria">Nigeria</option>
                        <option value="Niue">Niue</option>
                        <option value="Norfolk Island">Norfolk Island</option>
                        <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                        <option value="Norway">Norway</option>
                        <option value="Oman">Oman</option>
                        <option value="Pakistan">Pakistan</option>
                        <option value="Palau">Palau</option>
                        <option value="Panama">Panama</option>
                        <option value="Papua New Guinea">Papua New Guinea</option>
                        <option value="Paraguay">Paraguay</option>
                        <option value="Peru">Peru</option>
                        <option value="Philippines">Philippines</option>
                        <option value="Pitcairn">Pitcairn</option>
                        <option value="Poland">Poland</option>
                        <option value="Portugal">Portugal</option>
                        <option value="Puerto Rico">Puerto Rico</option>
                        <option value="Qatar">Qatar</option>
                        <option value="Reunion">Reunion</option>
                        <option value="Romania">Romania</option>
                        <option value="Russian Federation">Russian Federation</option>
                        <option value="Rwanda">Rwanda</option>
                        <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                        <option value="Saint Martin">Saint Martin</option>
                        <option value="Saint Lucia">Saint Lucia</option>
                        <option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
                        <option value="Samoa">Samoa</option>
                        <option value="San Marino">San Marino</option>
                        <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                        <option value="Saudi Arabia">Saudi Arabia</option>
                        <option value="Senegal">Senegal</option>
                        <option value="Seychelles">Seychelles</option>
                        <option value="Serbia">Serbia</option>
                        <option value="Sierra Leone">Sierra Leone</option>
                        <option value="Singapore">Singapore</option>
                        <option value="Slovakia (Slovak Republic)">Slovakia (Slovak Republic)</option>
                        <option value="Slovenia">Slovenia</option>
                        <option value="Solomon Islands">Solomon Islands</option>
                        <option value="Somalia">Somalia</option>
                        <option value="South Africa">South Africa</option>
                        <option value="South Georgia and the South Sandwich Islands">South Georgia and the South Sandwich Islands</option>
                        <option value="Spain">Spain</option>
                        <option value="Sri Lanka">Sri Lanka</option>
                        <option value="St. Helena">St. Helena</option>
                        <option value="St. Pierre and Miquelon">St. Pierre and Miquelon</option>
                        <option value="Sudan">Sudan</option>
                        <option value="Suriname">Suriname</option>
                        <option value="Svalbard and Jan Mayen Islands">Svalbard and Jan Mayen Islands</option>
                        <option value="Swaziland">Swaziland</option>
                        <option value="Sweden">Sweden</option>
                        <option value="Switzerland">Switzerland</option>
                        <option value="Syrian Arab Republic">Syrian Arab Republic</option>
                        <option value="Taiwan, Province of China">Taiwan, Province of China</option>
                        <option value="Tajikistan">Tajikistan</option>
                        <option value="Tanzania, United Republic of">Tanzania, United Republic of</option>
                        <option value="Thailand">Thailand</option>
                        <option value="Togo">Togo</option>
                        <option value="Tokelau">Tokelau</option>
                        <option value="Tonga">Tonga</option>
                        <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                        <option value="Tunisia">Tunisia</option>
                        <option value="Turkey">Turkey</option>
                        <option value="Turkmenistan">Turkmenistan</option>
                        <option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
                        <option value="Tuvalu">Tuvalu</option>
                        <option value="Uganda">Uganda</option>
                        <option value="Ukraine">Ukraine</option>
                        <option value="United Arab Emirates">United Arab Emirates</option>
                        <option value="United Kingdom">United Kingdom</option>
                        <option value="United States" selected="selected">United States</option>
                        <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                        <option value="Uruguay">Uruguay</option>
                        <option value="Uzbekistan">Uzbekistan</option>
                        <option value="Vanuatu">Vanuatu</option>
                        <option value="Venezuela">Venezuela</option>
                        <option value="Vietnam">Vietnam</option>
                        <option value="Virgin Islands (British)">Virgin Islands (British)</option>
                        <option value="Virgin Islands (U.S.)">Virgin Islands (U.S.)</option>
                        <option value="Wallis and Futuna Islands">Wallis and Futuna Islands</option>
                        <option value="Western Sahara">Western Sahara</option>
                        <option value="Yemen">Yemen</option>
                        <option value="Zambia">Zambia</option>
                        <option value="Zimbabwe">Zimbabwe</option>
                     </select>
                  </p>
               </div>
            </div>
         </div>
         <div class="col-md-12 add-estate profile-page profile-onprofile row">
            <div class="submit_container">
               <div class="row">
                  <div>
				    <p class="half_form ">
                       <label for="between-floors">Floors numbers(*text) </label><input type="text" id="num_floors" size="40" name="num_floors" value="">
                     </p>
                     <p class="half_form ">
                        <label for="property_bedrooms ">Bedrooms numbers(*only numbers)</label>
                        <input type="text" id="property_bedrooms" size="40" class="form-control" name="property_bedrooms" value="">
                     </p>
                     <p class="half_form ">
                        <label class="form-label form-label-top form-label-auto" id="label_31" for="input_31">
                        Area unit
                        <span class="form-required">
                        *
                        </span>
                        </label>
                     <div id="cid_31" class="form-input-wide jf-required">
                        <select class="form-dropdown validate[required]" id="input_31" name="q31_areaUnit" style="width:150px" data-component="dropdown" required="">
                           <option value="">  </option>
                           <option selected="" value="Square Feet"> Square Feet </option>
                           <option value="Square Meter"> Square Meter </option>
                        </select>
                     </div>
                  </div>
                  <!-- Add custom details -->
                  </p>
				  
				  <p class="half_form ">
                     <label class="form-label form-label-top form-label-auto" id="label_43" for="input_43">
                     Plot area
                     <span class="form-required">
                     *
                     </span>
                     </label>
                  <div id="cid_43" class="form-input-wide jf-required">
                     <input type="number" id="input_43" name="q43_plotArea" data-type="input-number" class=" form-number-input form-textbox validate[required]" style="width:60px" size="5" value="" data-component="number" required="">
                  </div>
                  <div class="form-description" style="display: none;">
                     <div class="form-description-arrow"></div>
                     <div class="form-description-arrow-small"></div>
                     <div class="form-description-content">Plot area</div>
                  </div>
                  </p>
                  <p class="half_form ">
                     <label class="form-label form-label-top form-label-auto" id="label_42" for="input_42">
                     Built-up area of the property
                     <span class="form-required">
                     *
                     </span>
                     </label>
                  <div id="cid_42" class="form-input-wide jf-required">
                     <input type="number" id="input_42" name="q42_builtupArea" data-type="input-number" class=" form-number-input form-textbox validate[required]" style="width:60px" size="5" value="" data-component="number" required="">
                  </div>
                  <div class="form-description" style="display: none;">
                     <div class="form-description-arrow"></div>
                     <div class="form-description-arrow-small"></div>
                     <div class="form-description-content">Built-up area of the property</div>
                  </div>
                  </p>
                  
                  <p class="half_form ">
                     <label class="form-label form-label-top form-label-auto" id="label_49" for="input_49">
                     Currency
                     <span class="form-required">
                     *
                     </span>
                     </label>
                  <div id="cid_49" class="form-input-wide jf-required">
                     <select class="form-dropdown validate[required]" id="input_49" name="q49_askingPrice49" style="width:150px" data-component="dropdown" required="">
                        <option value="">  </option>
                        <option value="Local Currency"> Local Currency </option>
                        <option value="US Dollars"> US Dollars </option>
                     </select>
                  </div>
                  </p>
                  <p class="half_form ">
                     <label class="form-label form-label-top form-label-auto" id="label_44" for="input_44">
                     Asking price
                     <span class="form-required">
                     *
                     </span>
                     </label>
                  <div id="cid_44" class="form-input-wide jf-required">
                     <span class="form-sub-label-container" style="vertical-align:top">
                     <input type="number" id="input_44" name="q44_askingPrice" data-type="input-number" class=" form-number-input form-textbox validate[required]" style="width:116px" size="12" value="" placeholder="ex: 2,000,000" data-component="number" required="">
                     <label class="form-sub-label" for="input_44" style="min-height:13px"> Enter the expected price of the property </label>
                     </span>
                  </div>
                  </p>
                  <p class="half_form ">
                  </p>
               </div>
            </div>
         </div>
         <div class="submit_container ">
            <div class="col-md-4 profile_label">
               <!--<div class="submit_container_header">Amenities and Features</div>-->
               <div class="user_details_row">Sellers or property renters who are authorized to contact me *</div>
            </div>
            <div class="col-md-8">
               <label class="form-label form-label-top form-label-auto" id="label_34" for="input_34_0">
               Sellers or property renters who are authorized to contact me
               <span class="form-required">
               *
               </span>
               </label>
               <div id="cid_34" class="form-input-wide jf-required">
                  <div class="form-single-column" data-component="checkbox">
                     <span class="form-checkbox-item" style="clear:left">
                     <span class="dragger-item">
                     </span>
                     <input type="checkbox" class="form-checkbox validate[required]" id="input_34_0" name="q34_sellersOr34[]" value="Individual sellers" required="">
                     <label id="label_input_34_0" for="input_34_0"> Individual sellers </label>
                     </span>
                     <span class="form-checkbox-item" style="clear:left">
                     <span class="dragger-item">
                     </span>
                     <input type="checkbox" class="form-checkbox validate[required]" id="input_34_1" name="q34_sellersOr34[]" value="Real estate agent free of charge (buyers not paying commission)" required="">
                     <label id="label_input_34_1" for="input_34_1"> Real estate agent free of charge (buyers not paying commission) </label>
                     </span>
                     <span class="form-checkbox-item" style="clear:left">
                     <span class="dragger-item">
                     </span>
                     <input type="checkbox" class="form-checkbox validate[required]" id="input_34_2" name="q34_sellersOr34[]" value="Real Estate agent who charges commission" required="">
                     <label id="label_input_34_2" for="input_34_2"> Real Estate agent who charges commission </label>
                     </span>
                     <span class="form-checkbox-item" style="clear:left">
                     <span class="dragger-item">
                     </span>
                     <input type="checkbox" class="form-checkbox validate[required]" id="input_34_3" name="q34_sellersOr34[]" value="Real Estate agents who represent an asset that is theirs in exclusivity." required="">
                     <label id="label_input_34_3" for="input_34_3"> Real Estate agents who represent an asset that is theirs in exclusivity. </label>
                     </span>
                     <span class="form-checkbox-item" style="clear:left">
                     <span class="dragger-item">
                     </span>
                     <input type="checkbox" class="form-checkbox validate[required]" id="input_34_4" name="q34_sellersOr34[]" value="A real estate agent who represents the property without exclusivity." required="">
                     <label id="label_input_34_4" for="input_34_4"> A real estate agent who represents the property without exclusivity. </label>
                     </span>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-md-12">
               <label class="form-label form-label-top form-label-auto" id="label_47" for="input_47"> Property photo(s) </label>
               <div id="cid_47" class="form-input-wide">
                  <div data-wrapper-react="true" class="validate[multipleUpload]">
                     <div class="qq-uploader">
                        <div class="qq-upload-drop-area" style="display: none;"><span>Drop files here to upload</span></div>
                        <div class="qq-upload-button" style="position: relative; overflow: hidden; direction: ltr;">
                           Browse files
                        </div>
                        <div class="inputContainer" style="overflow: hidden; position: absolute; top: 0px; right: 0px; width: 224px; height: 36px;"><input multiple="multiple" type="file" name="file" style="position: absolute; right: 0px; top: 0px; font-family: Arial; font-size: 118px; margin: 0px; padding: 0px; cursor: pointer; opacity: 0;"></div>
                        <div class="form-sub-label"> Max 10 images </div>
                        <span style="display:none" class="multipleFileUploadLabels cancelText">undefined</span><span style="display:none" class="multipleFileUploadLabels ofText">undefined</span>
                        <ul class="qq-upload-list"></ul>
                     </div>
                  </div>
               </div>
			    <label class="form-label form-label-top" id="label_12" for="input_12"> Anything else to say? </label>
        <div id="cid_12" class="form-input-wide">
          <div class="form-textarea-limit">
            <span>
              <textarea id="input_12" class="form-textarea" name="q12_anythingElse" cols="60" rows="6" data-component="textarea"></textarea>
              <div class="form-textarea-limit-indicator">
                <span data-limit="120" type="Words" data-minimum="-1" data-typeminimum="None" id="input_12-limit">0/120</span>
              </div>
            </span>
          </div>
        </div>
            </div>
         </div>
      </div>
      <div class="col-md-3 user_dashboard"> 
      </div>
      <div class="row">
         <div class="col-md-12">
            <div class="col-md-3"></div>
            <div class="col-md-9">
               <input type="hidden" name="action" value="view">
               <div class="submit_form_row">
                  <input type="submit" class="wpresidence_button" id="form_submit_1" value="ADD PROPERTY REQUEST">
               </div>
            </div>
         </div>
      </div>
   </div>
</form>
	<?php
$myvariable = ob_get_clean();
return $myvariable;
}
function wp_request_listing_form($atts) {
  if(isset($_POST['submit_request'])){
  $formID = $_POST['formID'];
    $input_language = $_POST['input_language'];
    $smsConfirmation = $_POST['smsConfirmation'];
    $my_role = $_POST['my_role'];
    $countries = $_POST['countries'];
    $city = $_POST['city'];
    $neighborhood = $_POST['neighborhood'];
    $property_type = $_POST['property_type'];
    $required_features = $_POST['required_features'];
    $minimum_bedrooms = $_POST['minimum_bedrooms'];
    $between_floors = $_POST['between_floors']; 
    $area_unit = $_POST['area_unit']; 
    $currency_type = $_POST['currency_type']; 
    $min_budget = $_POST['min_budget'];
    $maximum_budget = $_POST['maximum_budget'];
    $extra_requirments = $_POST['extra_requirments'];
    $authorized_to = $_POST['authorized_to'];
    $full_name = $_POST['full_name'];
    $user_email = $_POST['user_email']; 
    $mobile_number = $_POST['mobile_number'];
  $my_post = array(
  'post_title'  => 'My post',
  'post_type'   => 'buyer_requestrequest',
  'post_status' => 'publish'
);


// insert the post into the database
$request_id = wp_insert_post( $my_post );


// save a basic text value
$field_key = "city";
$value = "some new string";
update_field( $field_key, $value, $request_id );
  }
$userid ='user_7';
$user_data11 =  get_userdata( $userid );
update_field( 'admin_location', 'New York', $userid );

$user_data = get_user_meta($userid, 'dasdad');
?><pre><?php print_r('$user_data'); ?> </pre><?php
?><pre><?php print_r($user_data11); ?> </pre><?php
?><pre><?php print_r($user_data); ?> </pre><?php
  ?>
<form class="jotform-form" action="" method="post" name="" id="">
   <input type="hidden" name="formID" value="81075632891461">
   <div class="form-all">
      <div class="progressBarContainer">
         <div id="progressBarWidget" data-progressjs="1">
            <div class="progressjs-container">
               <div class="progressjs-progress progressjs-theme-lampBlack" data-progressjs="1" style="width: 100%; height: 20px;">
                  <div class="progressjs-inner" style="width: 21%;">
                     <div class="progressjs-percent">21%</div>
                  </div>
               </div>
            </div>
         </div>
         <div class="progressBarSubtitle">        <span id="progressPercentage">21% </span>        <span id="progressCompleted" style="margin-left:10px">4</span>        <span>/</span>        <span id="progressTotal">19</span> <span id="status_text1">Fields Completed.</span>         <span id="progressSubmissionReminder" style="display:none; margin-left: 10px;">Please Submit the Form.</span>    </div>
      </div>
      <link type="text/css" rel="stylesheet" media="all" href="https://cdn.jotfor.ms/wizards/languageWizard/custom-dropdown/css/lang-dd.css?3.3.6083">
      <div class="cont">
         <input type="text" id="input_language" name="input_language" style="display:none">
         <div class="language-dd" id="langDd" style="display:none">
            <div class="dd-placeholder en">English (US)</div>
            <ul class="lang-list dn light" id="langList">
               <li data-lang="en" class="en">
                  English (US)
               </li>
            </ul>
         </div>
      </div>
      <script type="text/javascript" src="https://cdn.jotfor.ms/js/formTranslation.v2.js?3.3.6083"></script>
      <ul class="form-section page-section">
         <li id="cid_1" class="form-input-wide" data-type="control_head">
            <div class="form-header-group ">
               <div class="header-text httal htvam">
                  <h1 id="header_1" class="form-header" data-component="header">
                     Billboard for Buyers and Tenants (2)
                  </h1>
                  <div id="subHeader_1" class="form-subHeader">
                     Requested Property
                  </div>
               </div>
            </div>
         </li>
         <li class="form-line" data-type="control_widget" id="id_30">
            <label class="form-label form-label-top" id="label_30" for="input_30"> SMS confirmation </label>
            <div id="cid_30" class="form-input-wide">
               <div style="width:100%;text-align:Left" data-component="widget-field">
                  <iframe frameborder="0" scrolling="no" allowtransparency="true" allow="geolocation; microphone; camera" data-type="iframe" class="custom-field-frame custom-field-frame-rendered frame-xd-ready frame-ready" id="customFieldFrame_30" src="//data-widgets.jotform.io/smsConfirmation/?qid=30&amp;ref=https%3A%2F%2Fwww.jotform.com" style="border: none; width: 360px; height: 24px;" data-width="25" data-height="100">
                  </iframe>
                  <div class="widget-inputs-wrapper">
                     <input type="hidden" id="input_30" class="form-hidden form-widget" name="smsConfirmation" value="">
                     <input type="hidden" id="widget_settings_30" class="form-hidden form-widget-settings" value="%5B%7B%22name%22%3A%22defaultCountry%22%2C%22value%22%3A%22us%22%7D%2C%7B%22name%22%3A%22smsTpl%22%2C%22value%22%3A%22Hello%2C%20Your%20confirmation%20code%20is%20%7Bcode%7D.%22%7D%2C%7B%22name%22%3A%22confirm%22%2C%22value%22%3A%22Confirm%22%7D%2C%7B%22name%22%3A%22error%22%2C%22value%22%3A%22Something%20went%20wrong.%20Please%20try%20again.%20%22%7D%5D" data-version="2">
                  </div>
               </div>
            </div>
         </li>
         <li class="form-line jf-required" data-type="control_dropdown" id="id_18" data-progress="true">
            <label class="form-label form-label-top form-label-auto" id="label_18" for="input_18">
            I am
            <span class="form-required">
            *
            </span></label>
            <div id="cid_18" class="form-input-wide jf-required">
               <select class="form-dropdown validate[required]" id="input_18" name="my_role" style="width:150px" data-component="dropdown" required="">
                  <option value=""></option>
                  <option selected="" value="A buyer"> A buyer </option>
                  <option value="A tenant"> A tenant </option>
               </select>
            </div>
         </li>
         <li class="form-line" data-type="control_widget" id="id_17">
            <label class="form-label form-label-top form-label-auto" id="label_17" for="input_17"> Select your desired location </label>
            <div id="cid_17" class="form-input-wide">
               <div style="width:100%;text-align:Left" data-component="widget-field">
                  <select name="countries">
                     <option value=""></option>
                     <option value="1" data-sortname="AF" data-name="Afghanistan">Afghanistan</option>
                     <option value="2" data-sortname="AL" data-name="Albania">Albania</option>
                     <option value="3" data-sortname="DZ" data-name="Algeria">Algeria</option>
                     <option value="4" data-sortname="AS" data-name="American Samoa">American Samoa</option>
                     <option value="5" data-sortname="AD" data-name="Andorra">Andorra</option>
                     <option value="6" data-sortname="AO" data-name="Angola">Angola</option>
                     <option value="7" data-sortname="AI" data-name="Anguilla">Anguilla</option>
                     <option value="8" data-sortname="AQ" data-name="Antarctica">Antarctica</option>
                     <option value="9" data-sortname="AG" data-name="Antigua And Barbuda">Antigua And Barbuda</option>
                     <option value="10" data-sortname="AR" data-name="Argentina">Argentina</option>
                     <option value="11" data-sortname="AM" data-name="Armenia">Armenia</option>
                     <option value="12" data-sortname="AW" data-name="Aruba">Aruba</option>
                     <option value="13" data-sortname="AU" data-name="Australia">Australia</option>
                     <option value="14" data-sortname="AT" data-name="Austria">Austria</option>
                     <option value="15" data-sortname="AZ" data-name="Azerbaijan">Azerbaijan</option>
                     <option value="16" data-sortname="BS" data-name="Bahamas The">Bahamas The</option>
                     <option value="17" data-sortname="BH" data-name="Bahrain">Bahrain</option>
                     <option value="18" data-sortname="BD" data-name="Bangladesh">Bangladesh</option>
                     <option value="19" data-sortname="BB" data-name="Barbados">Barbados</option>
                     <option value="20" data-sortname="BY" data-name="Belarus">Belarus</option>
                     <option value="21" data-sortname="BE" data-name="Belgium">Belgium</option>
                     <option value="22" data-sortname="BZ" data-name="Belize">Belize</option>
                     <option value="23" data-sortname="BJ" data-name="Benin">Benin</option>
                     <option value="24" data-sortname="BM" data-name="Bermuda">Bermuda</option>
                     <option value="25" data-sortname="BT" data-name="Bhutan">Bhutan</option>
                     <option value="26" data-sortname="BO" data-name="Bolivia">Bolivia</option>
                     <option value="27" data-sortname="BA" data-name="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                     <option value="28" data-sortname="BW" data-name="Botswana">Botswana</option>
                     <option value="29" data-sortname="BV" data-name="Bouvet Island">Bouvet Island</option>
                     <option value="30" data-sortname="BR" data-name="Brazil">Brazil</option>
                     <option value="31" data-sortname="IO" data-name="British Indian Ocean Territory">British Indian Ocean Territory</option>
                     <option value="32" data-sortname="BN" data-name="Brunei">Brunei</option>
                     <option value="33" data-sortname="BG" data-name="Bulgaria">Bulgaria</option>
                     <option value="34" data-sortname="BF" data-name="Burkina Faso">Burkina Faso</option>
                     <option value="35" data-sortname="BI" data-name="Burundi">Burundi</option>
                     <option value="36" data-sortname="KH" data-name="Cambodia">Cambodia</option>
                     <option value="37" data-sortname="CM" data-name="Cameroon">Cameroon</option>
                     <option value="38" data-sortname="CA" data-name="Canada">Canada</option>
                     <option value="39" data-sortname="CV" data-name="Cape Verde">Cape Verde</option>
                     <option value="40" data-sortname="KY" data-name="Cayman Islands">Cayman Islands</option>
                     <option value="41" data-sortname="CF" data-name="Central African Republic">Central African Republic</option>
                     <option value="42" data-sortname="TD" data-name="Chad">Chad</option>
                     <option value="43" data-sortname="CL" data-name="Chile">Chile</option>
                     <option value="44" data-sortname="CN" data-name="China">China</option>
                     <option value="45" data-sortname="CX" data-name="Christmas Island">Christmas Island</option>
                     <option value="46" data-sortname="CC" data-name="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                     <option value="47" data-sortname="CO" data-name="Colombia">Colombia</option>
                     <option value="48" data-sortname="KM" data-name="Comoros">Comoros</option>
                     <option value="49" data-sortname="CG" data-name="Congo">Congo</option>
                     <option value="50" data-sortname="CD" data-name="Congo The Democratic Republic Of The">Congo The Democratic Republic Of The</option>
                     <option value="51" data-sortname="CK" data-name="Cook Islands">Cook Islands</option>
                     <option value="52" data-sortname="CR" data-name="Costa Rica">Costa Rica</option>
                     <option value="53" data-sortname="CI" data-name="Cote D''Ivoire (Ivory Coast)">Cote D''Ivoire (Ivory Coast)</option>
                     <option value="54" data-sortname="HR" data-name="Croatia (Hrvatska)">Croatia (Hrvatska)</option>
                     <option value="55" data-sortname="CU" data-name="Cuba">Cuba</option>
                     <option value="56" data-sortname="CY" data-name="Cyprus">Cyprus</option>
                     <option value="57" data-sortname="CZ" data-name="Czech Republic">Czech Republic</option>
                     <option value="58" data-sortname="DK" data-name="Denmark">Denmark</option>
                     <option value="59" data-sortname="DJ" data-name="Djibouti">Djibouti</option>
                     <option value="60" data-sortname="DM" data-name="Dominica">Dominica</option>
                     <option value="61" data-sortname="DO" data-name="Dominican Republic">Dominican Republic</option>
                     <option value="62" data-sortname="TP" data-name="East Timor">East Timor</option>
                     <option value="63" data-sortname="EC" data-name="Ecuador">Ecuador</option>
                     <option value="64" data-sortname="EG" data-name="Egypt">Egypt</option>
                     <option value="65" data-sortname="SV" data-name="El Salvador">El Salvador</option>
                     <option value="66" data-sortname="GQ" data-name="Equatorial Guinea">Equatorial Guinea</option>
                     <option value="67" data-sortname="ER" data-name="Eritrea">Eritrea</option>
                     <option value="68" data-sortname="EE" data-name="Estonia">Estonia</option>
                     <option value="69" data-sortname="ET" data-name="Ethiopia">Ethiopia</option>
                     <option value="70" data-sortname="XA" data-name="External Territories of Australia">External Territories of Australia</option>
                     <option value="71" data-sortname="FK" data-name="Falkland Islands">Falkland Islands</option>
                     <option value="72" data-sortname="FO" data-name="Faroe Islands">Faroe Islands</option>
                     <option value="73" data-sortname="FJ" data-name="Fiji Islands">Fiji Islands</option>
                     <option value="74" data-sortname="FI" data-name="Finland">Finland</option>
                     <option value="75" data-sortname="FR" data-name="France">France</option>
                     <option value="76" data-sortname="GF" data-name="French Guiana">French Guiana</option>
                     <option value="77" data-sortname="PF" data-name="French Polynesia">French Polynesia</option>
                     <option value="78" data-sortname="TF" data-name="French Southern Territories">French Southern Territories</option>
                     <option value="79" data-sortname="GA" data-name="Gabon">Gabon</option>
                     <option value="80" data-sortname="GM" data-name="Gambia The">Gambia The</option>
                     <option value="81" data-sortname="GE" data-name="Georgia">Georgia</option>
                     <option value="82" data-sortname="DE" data-name="Germany">Germany</option>
                     <option value="83" data-sortname="GH" data-name="Ghana">Ghana</option>
                     <option value="84" data-sortname="GI" data-name="Gibraltar">Gibraltar</option>
                     <option value="85" data-sortname="GR" data-name="Greece">Greece</option>
                     <option value="86" data-sortname="GL" data-name="Greenland">Greenland</option>
                     <option value="87" data-sortname="GD" data-name="Grenada">Grenada</option>
                     <option value="88" data-sortname="GP" data-name="Guadeloupe">Guadeloupe</option>
                     <option value="89" data-sortname="GU" data-name="Guam">Guam</option>
                     <option value="90" data-sortname="GT" data-name="Guatemala">Guatemala</option>
                     <option value="91" data-sortname="XU" data-name="Guernsey and Alderney">Guernsey and Alderney</option>
                     <option value="92" data-sortname="GN" data-name="Guinea">Guinea</option>
                     <option value="93" data-sortname="GW" data-name="Guinea-Bissau">Guinea-Bissau</option>
                     <option value="94" data-sortname="GY" data-name="Guyana">Guyana</option>
                     <option value="95" data-sortname="HT" data-name="Haiti">Haiti</option>
                     <option value="96" data-sortname="HM" data-name="Heard and McDonald Islands">Heard and McDonald Islands</option>
                     <option value="97" data-sortname="HN" data-name="Honduras">Honduras</option>
                     <option value="98" data-sortname="HK" data-name="Hong Kong S.A.R.">Hong Kong S.A.R.</option>
                     <option value="99" data-sortname="HU" data-name="Hungary">Hungary</option>
                     <option value="100" data-sortname="IS" data-name="Iceland">Iceland</option>
                     <option value="101" data-sortname="IN" data-name="India">India</option>
                     <option value="102" data-sortname="ID" data-name="Indonesia">Indonesia</option>
                     <option value="103" data-sortname="IR" data-name="Iran">Iran</option>
                     <option value="104" data-sortname="IQ" data-name="Iraq">Iraq</option>
                     <option value="105" data-sortname="IE" data-name="Ireland">Ireland</option>
                     <option value="106" data-sortname="IL" data-name="Israel">Israel</option>
                     <option value="107" data-sortname="IT" data-name="Italy">Italy</option>
                     <option value="108" data-sortname="JM" data-name="Jamaica">Jamaica</option>
                     <option value="109" data-sortname="JP" data-name="Japan">Japan</option>
                     <option value="110" data-sortname="XJ" data-name="Jersey">Jersey</option>
                     <option value="111" data-sortname="JO" data-name="Jordan">Jordan</option>
                     <option value="112" data-sortname="KZ" data-name="Kazakhstan">Kazakhstan</option>
                     <option value="113" data-sortname="KE" data-name="Kenya">Kenya</option>
                     <option value="114" data-sortname="KI" data-name="Kiribati">Kiribati</option>
                     <option value="115" data-sortname="KP" data-name="Korea North">Korea North</option>
                     <option value="116" data-sortname="KR" data-name="Korea South">Korea South</option>
                     <option value="117" data-sortname="KW" data-name="Kuwait">Kuwait</option>
                     <option value="118" data-sortname="KG" data-name="Kyrgyzstan">Kyrgyzstan</option>
                     <option value="119" data-sortname="LA" data-name="Laos">Laos</option>
                     <option value="120" data-sortname="LV" data-name="Latvia">Latvia</option>
                     <option value="121" data-sortname="LB" data-name="Lebanon">Lebanon</option>
                     <option value="122" data-sortname="LS" data-name="Lesotho">Lesotho</option>
                     <option value="123" data-sortname="LR" data-name="Liberia">Liberia</option>
                     <option value="124" data-sortname="LY" data-name="Libya">Libya</option>
                     <option value="125" data-sortname="LI" data-name="Liechtenstein">Liechtenstein</option>
                     <option value="126" data-sortname="LT" data-name="Lithuania">Lithuania</option>
                     <option value="127" data-sortname="LU" data-name="Luxembourg">Luxembourg</option>
                     <option value="128" data-sortname="MO" data-name="Macau S.A.R.">Macau S.A.R.</option>
                     <option value="129" data-sortname="MK" data-name="Macedonia">Macedonia</option>
                     <option value="130" data-sortname="MG" data-name="Madagascar">Madagascar</option>
                     <option value="131" data-sortname="MW" data-name="Malawi">Malawi</option>
                     <option value="132" data-sortname="MY" data-name="Malaysia">Malaysia</option>
                     <option value="133" data-sortname="MV" data-name="Maldives">Maldives</option>
                     <option value="134" data-sortname="ML" data-name="Mali">Mali</option>
                     <option value="135" data-sortname="MT" data-name="Malta">Malta</option>
                     <option value="136" data-sortname="XM" data-name="Man (Isle of)">Man (Isle of)</option>
                     <option value="137" data-sortname="MH" data-name="Marshall Islands">Marshall Islands</option>
                     <option value="138" data-sortname="MQ" data-name="Martinique">Martinique</option>
                     <option value="139" data-sortname="MR" data-name="Mauritania">Mauritania</option>
                     <option value="140" data-sortname="MU" data-name="Mauritius">Mauritius</option>
                     <option value="141" data-sortname="YT" data-name="Mayotte">Mayotte</option>
                     <option value="142" data-sortname="MX" data-name="Mexico">Mexico</option>
                     <option value="143" data-sortname="FM" data-name="Micronesia">Micronesia</option>
                     <option value="144" data-sortname="MD" data-name="Moldova">Moldova</option>
                     <option value="145" data-sortname="MC" data-name="Monaco">Monaco</option>
                     <option value="146" data-sortname="MN" data-name="Mongolia">Mongolia</option>
                     <option value="147" data-sortname="MS" data-name="Montserrat">Montserrat</option>
                     <option value="148" data-sortname="MA" data-name="Morocco">Morocco</option>
                     <option value="149" data-sortname="MZ" data-name="Mozambique">Mozambique</option>
                     <option value="150" data-sortname="MM" data-name="Myanmar">Myanmar</option>
                     <option value="151" data-sortname="NA" data-name="Namibia">Namibia</option>
                     <option value="152" data-sortname="NR" data-name="Nauru">Nauru</option>
                     <option value="153" data-sortname="NP" data-name="Nepal">Nepal</option>
                     <option value="154" data-sortname="AN" data-name="Netherlands Antilles">Netherlands Antilles</option>
                     <option value="155" data-sortname="NL" data-name="Netherlands The">Netherlands The</option>
                     <option value="156" data-sortname="NC" data-name="New Caledonia">New Caledonia</option>
                     <option value="157" data-sortname="NZ" data-name="New Zealand">New Zealand</option>
                     <option value="158" data-sortname="NI" data-name="Nicaragua">Nicaragua</option>
                     <option value="159" data-sortname="NE" data-name="Niger">Niger</option>
                     <option value="160" data-sortname="NG" data-name="Nigeria">Nigeria</option>
                     <option value="161" data-sortname="NU" data-name="Niue">Niue</option>
                     <option value="162" data-sortname="NF" data-name="Norfolk Island">Norfolk Island</option>
                     <option value="163" data-sortname="MP" data-name="Northern Mariana Islands">Northern Mariana Islands</option>
                     <option value="164" data-sortname="NO" data-name="Norway">Norway</option>
                     <option value="165" data-sortname="OM" data-name="Oman">Oman</option>
                     <option value="166" data-sortname="PK" data-name="Pakistan">Pakistan</option>
                     <option value="167" data-sortname="PW" data-name="Palau">Palau</option>
                     <option value="168" data-sortname="PS" data-name="Palestinian Territory Occupied">Palestinian Territory Occupied</option>
                     <option value="169" data-sortname="PA" data-name="Panama">Panama</option>
                     <option value="170" data-sortname="PG" data-name="Papua new Guinea">Papua new Guinea</option>
                     <option value="171" data-sortname="PY" data-name="Paraguay">Paraguay</option>
                     <option value="172" data-sortname="PE" data-name="Peru">Peru</option>
                     <option value="173" data-sortname="PH" data-name="Philippines">Philippines</option>
                     <option value="174" data-sortname="PN" data-name="Pitcairn Island">Pitcairn Island</option>
                     <option value="175" data-sortname="PL" data-name="Poland">Poland</option>
                     <option value="176" data-sortname="PT" data-name="Portugal">Portugal</option>
                     <option value="177" data-sortname="PR" data-name="Puerto Rico">Puerto Rico</option>
                     <option value="178" data-sortname="QA" data-name="Qatar">Qatar</option>
                     <option value="179" data-sortname="RE" data-name="Reunion">Reunion</option>
                     <option value="180" data-sortname="RO" data-name="Romania">Romania</option>
                     <option value="181" data-sortname="RU" data-name="Russia">Russia</option>
                     <option value="182" data-sortname="RW" data-name="Rwanda">Rwanda</option>
                     <option value="183" data-sortname="SH" data-name="Saint Helena">Saint Helena</option>
                     <option value="184" data-sortname="KN" data-name="Saint Kitts And Nevis">Saint Kitts And Nevis</option>
                     <option value="185" data-sortname="LC" data-name="Saint Lucia">Saint Lucia</option>
                     <option value="186" data-sortname="PM" data-name="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
                     <option value="187" data-sortname="VC" data-name="Saint Vincent And The Grenadines">Saint Vincent And The Grenadines</option>
                     <option value="188" data-sortname="WS" data-name="Samoa">Samoa</option>
                     <option value="189" data-sortname="SM" data-name="San Marino">San Marino</option>
                     <option value="190" data-sortname="ST" data-name="Sao Tome and Principe">Sao Tome and Principe</option>
                     <option value="191" data-sortname="SA" data-name="Saudi Arabia">Saudi Arabia</option>
                     <option value="192" data-sortname="SN" data-name="Senegal">Senegal</option>
                     <option value="193" data-sortname="RS" data-name="Serbia">Serbia</option>
                     <option value="194" data-sortname="SC" data-name="Seychelles">Seychelles</option>
                     <option value="195" data-sortname="SL" data-name="Sierra Leone">Sierra Leone</option>
                     <option value="196" data-sortname="SG" data-name="Singapore">Singapore</option>
                     <option value="197" data-sortname="SK" data-name="Slovakia">Slovakia</option>
                     <option value="198" data-sortname="SI" data-name="Slovenia">Slovenia</option>
                     <option value="199" data-sortname="XG" data-name="Smaller Territories of the UK">Smaller Territories of the UK</option>
                     <option value="200" data-sortname="SB" data-name="Solomon Islands">Solomon Islands</option>
                     <option value="201" data-sortname="SO" data-name="Somalia">Somalia</option>
                     <option value="202" data-sortname="ZA" data-name="South Africa">South Africa</option>
                     <option value="203" data-sortname="GS" data-name="South Georgia">South Georgia</option>
                     <option value="204" data-sortname="SS" data-name="South Sudan">South Sudan</option>
                     <option value="205" data-sortname="ES" data-name="Spain">Spain</option>
                     <option value="206" data-sortname="LK" data-name="Sri Lanka">Sri Lanka</option>
                     <option value="207" data-sortname="SD" data-name="Sudan">Sudan</option>
                     <option value="208" data-sortname="SR" data-name="Suriname">Suriname</option>
                     <option value="209" data-sortname="SJ" data-name="Svalbard And Jan Mayen Islands">Svalbard And Jan Mayen Islands</option>
                     <option value="210" data-sortname="SZ" data-name="Swaziland">Swaziland</option>
                     <option value="211" data-sortname="SE" data-name="Sweden">Sweden</option>
                     <option value="212" data-sortname="CH" data-name="Switzerland">Switzerland</option>
                     <option value="213" data-sortname="SY" data-name="Syria">Syria</option>
                     <option value="214" data-sortname="TW" data-name="Taiwan">Taiwan</option>
                     <option value="215" data-sortname="TJ" data-name="Tajikistan">Tajikistan</option>
                     <option value="216" data-sortname="TZ" data-name="Tanzania">Tanzania</option>
                     <option value="217" data-sortname="TH" data-name="Thailand">Thailand</option>
                     <option value="218" data-sortname="TG" data-name="Togo">Togo</option>
                     <option value="219" data-sortname="TK" data-name="Tokelau">Tokelau</option>
                     <option value="220" data-sortname="TO" data-name="Tonga">Tonga</option>
                     <option value="221" data-sortname="TT" data-name="Trinidad And Tobago">Trinidad And Tobago</option>
                     <option value="222" data-sortname="TN" data-name="Tunisia">Tunisia</option>
                     <option value="223" data-sortname="TR" data-name="Turkey">Turkey</option>
                     <option value="224" data-sortname="TM" data-name="Turkmenistan">Turkmenistan</option>
                     <option value="225" data-sortname="TC" data-name="Turks And Caicos Islands">Turks And Caicos Islands</option>
                     <option value="226" data-sortname="TV" data-name="Tuvalu">Tuvalu</option>
                     <option value="227" data-sortname="UG" data-name="Uganda">Uganda</option>
                     <option value="228" data-sortname="UA" data-name="Ukraine">Ukraine</option>
                     <option value="229" data-sortname="AE" data-name="United Arab Emirates">United Arab Emirates</option>
                     <option value="230" data-sortname="GB" data-name="United Kingdom">United Kingdom</option>
                     <option value="231" data-sortname="US" data-name="United States">United States</option>
                     <option value="232" data-sortname="UM" data-name="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                     <option value="233" data-sortname="UY" data-name="Uruguay">Uruguay</option>
                     <option value="234" data-sortname="UZ" data-name="Uzbekistan">Uzbekistan</option>
                     <option value="235" data-sortname="VU" data-name="Vanuatu">Vanuatu</option>
                     <option value="236" data-sortname="VA" data-name="Vatican City State (Holy See)">Vatican City State (Holy See)</option>
                     <option value="237" data-sortname="VE" data-name="Venezuela">Venezuela</option>
                     <option value="238" data-sortname="VN" data-name="Vietnam">Vietnam</option>
                     <option value="239" data-sortname="VG" data-name="Virgin Islands (British)">Virgin Islands (British)</option>
                     <option value="240" data-sortname="VI" data-name="Virgin Islands (US)">Virgin Islands (US)</option>
                     <option value="241" data-sortname="WF" data-name="Wallis And Futuna Islands">Wallis And Futuna Islands</option>
                     <option value="242" data-sortname="EH" data-name="Western Sahara">Western Sahara</option>
                     <option value="243" data-sortname="YE" data-name="Yemen">Yemen</option>
                     <option value="244" data-sortname="YU" data-name="Yugoslavia">Yugoslavia</option>
                     <option value="245" data-sortname="ZM" data-name="Zambia">Zambia</option>
                     <option value="246" data-sortname="ZW" data-name="Zimbabwe">Zimbabwe</option>
                  </select>
               </div>
            </div>
         </li>
         <li class="form-line" data-type="control_textbox" id="id_41">
            <label class="form-label form-label-top form-label-auto" id="label_41" for="input_41"> City on the list? </label>
            <div id="cid_41" class="form-input-wide">
               <input type="text" id="input_41" name="city" data-type="input-textbox" class="form-textbox" size="20" value="" placeholder="Your City" data-component="textbox">
            </div>
         </li>
         <li class="form-line" data-type="control_textbox" id="id_46">
            <label class="form-label form-label-top form-label-auto" id="label_46" for="input_46"> Neighborhood (Option) </label>
            <div id="cid_46" class="form-input-wide">
               <input type="text" id="input_46" name="neighborhood" data-type="input-textbox" class="form-textbox" size="20" value="" data-component="textbox">
            </div>
         </li>
         <li class="form-line jf-required" data-type="control_dropdown" id="id_20" data-progress="true">
            <label class="form-label form-label-top form-label-auto" id="label_20" for="input_20">
            Property type
            <span class="form-required">
            *
            </span></label>
            <div id="cid_20" class="form-input-wide jf-required">
               <select class="form-dropdown validate[required]" id="input_20" name="property_type" style="width:150px" data-component="dropdown" required="">
                  <option value=""></option>
                  <option value="Apartment"> Apartment </option>
                  <option value="House"> House </option>
                  <option value="Penthouse"> Penthouse </option>
                  <option value="Duplex"> Duplex </option>
                  <option value="Studio"> Studio </option>
                  <option value="Garden Apartment"> Garden Apartment </option>
                  <option value="Unit"> Unit </option>
                  <option value="Top Apartment"> Top Apartment </option>
                  <option value="Loft"> Loft </option>
                  <option value="Studio"> Studio </option>
                  <option value="Vacation Apartment"> Vacation Apartment </option>
                  <option value="Basement"> Basement </option>
                  <option value="B &amp; B"> B &amp; B </option>
                  <option value="Farm"> Farm </option>
                  <option value="Storage"> Storage </option>
                  <option value="Parking Spot"> Parking Spot </option>
                  <option value="Nursing Home"> Nursing Home </option>
                  <option value="Plots"> Plots </option>
                  <option value="Building"> Building </option>
                  <option value="Various Asset"> Various Asset </option>
                  <option selected="" value=""></option>
               </select>
            </div>
         </li>
         <li class="form-line" data-type="control_checkbox" id="id_37" data-progress="false">
            <label class="form-label form-label-top form-label-auto" id="label_37" for="input_37_0"> Required Features </label>
            <div id="cid_37" class="form-input-wide">
               <div class="form-single-column" data-component="checkbox">
                  <span class="form-checkbox-item" style="clear:left">
                  <span class="dragger-item">
                  </span>
                  <input type="checkbox" class="form-checkbox" id="input_37_0" name="required_features[]" value="A. Parking Spot,">
                  <label id="label_input_37_0" for="input_37_0"> A. Parking Spot, </label>
                  </span>
                  <span class="form-checkbox-item" style="clear:left">
                  <span class="dragger-item">
                  </span>
                  <input type="checkbox" class="form-checkbox" id="input_37_1" name="required_features[]" value="B. Elevator,">
                  <label id="label_input_37_1" for="input_37_1"> B. Elevator, </label>
                  </span>
                  <span class="form-checkbox-item" style="clear:left">
                  <span class="dragger-item">
                  </span>
                  <input type="checkbox" class="form-checkbox" id="input_37_2" name="required_features[]" value="C. Air Condition,">
                  <label id="label_input_37_2" for="input_37_2"> C. Air Condition, </label>
                  </span>
                  <span class="form-checkbox-item" style="clear:left">
                  <span class="dragger-item">
                  </span>
                  <input type="checkbox" class="form-checkbox" id="input_37_3" name="required_features[]" value="D. Shelter,">
                  <label id="label_input_37_3" for="input_37_3"> D. Shelter, </label>
                  </span>
                  <span class="form-checkbox-item" style="clear:left">
                  <span class="dragger-item">
                  </span>
                  <input type="checkbox" class="form-checkbox" id="input_37_4" name="required_features[]" value="E. Renovated,">
                  <label id="label_input_37_4" for="input_37_4"> E. Renovated, </label>
                  </span>
                  <span class="form-checkbox-item" style="clear:left">
                  <span class="dragger-item">
                  </span>
                  <input type="checkbox" class="form-checkbox" id="input_37_5" name="required_features[]" value="F. Balcony,">
                  <label id="label_input_37_5" for="input_37_5"> F. Balcony, </label>
                  </span>
                  <span class="form-checkbox-item" style="clear:left">
                  <span class="dragger-item">
                  </span>
                  <input type="checkbox" class="form-checkbox" id="input_37_6" name="required_features[]" value="G. Sun terrace,">
                  <label id="label_input_37_6" for="input_37_6"> G. Sun terrace, </label>
                  </span>
                  <span class="form-checkbox-item" style="clear:left">
                  <span class="dragger-item">
                  </span>
                  <input type="checkbox" class="form-checkbox" id="input_37_7" name="required_features[]" value="H. Storage Room,">
                  <label id="label_input_37_7" for="input_37_7"> H. Storage Room, </label>
                  </span>
                  <span class="form-checkbox-item" style="clear:left">
                  <span class="dragger-item">
                  </span>
                  <input type="checkbox" class="form-checkbox" id="input_37_8" name="required_features[]" value="I. Disabled Access,">
                  <label id="label_input_37_8" for="input_37_8"> I. Disabled Access, </label>
                  </span>
               </div>
            </div>
         </li>
         <li class="form-line" data-type="control_number" id="id_49">
            <label class="form-label form-label-top form-label-auto" id="label_49" for="input_49"> Minimum - Maximum Bedroom </label>
            <div id="cid_49" class="form-input-wide">
               <input type="number" id="input_49" name="minimum_bedrooms" data-type="input-number" class=" form-number-input form-textbox" style="width:60px" size="5" value="" placeholder="ex: 3-5" data-component="number">
            </div>
         </li>
         <li class="form-line" data-type="control_number" id="id_42">
            <label class="form-label form-label-top form-label-auto" id="label_42" for="input_42"> Between floors </label>
            <div id="cid_42" class="form-input-wide">
               <input type="number" id="input_42" name="between_floors" data-type="input-number" class=" form-number-input form-textbox" style="width:100px" size="10" value="" placeholder="Floor: 2-5" data-component="number">
            </div>
         </li>
         <li class="form-line jf-required" data-type="control_dropdown" id="id_31" data-progress="true">
            <label class="form-label form-label-top form-label-auto" id="label_31" for="input_31">
            Area unit
            <span class="form-required">
            *
            </span></label>
            <div id="cid_31" class="form-input-wide jf-required">
               <select class="form-dropdown validate[required]" id="input_31" name="area_unit" style="width:150px" data-component="dropdown">
                  <option value=""></option>
                  <option selected="" value="Square Feet"> Square Feet </option>
                  <option value="Square Meter"> Square Meter </option>
               </select>
            </div>
         </li>
         <li class="form-line" data-type="control_dropdown" id="id_39" data-progress="true">
            <label class="form-label form-label-top form-label-auto" id="label_39" for="input_39"> Currency Type </label>
            <div id="cid_39" class="form-input-wide">
               <select class="form-dropdown" id="input_39" name="currency_type" style="width:150px" data-component="dropdown">
                  <option value=""></option>
                  <option selected="" value="Local Currency"> Local Currency </option>
                  <option value="US Dollar"> US Dollar </option>
               </select>
            </div>
         </li>
         <li class="form-line" data-type="control_number" id="id_47">
            <label class="form-label form-label-top form-label-auto" id="label_47" for="input_47"> Min Budget </label>
            <div id="cid_47" class="form-input-wide">
               <input type="number" id="input_47" name="min_budget" data-type="input-number" class=" form-number-input form-textbox" style="width:60px" size="5" value="" placeholder="ex: 23" data-component="number">
            </div>
         </li>
         <li class="form-line" data-type="control_number" id="id_48">
            <label class="form-label form-label-top form-label-auto" id="label_48" for="input_48"> Maximum Budget </label>
            <div id="cid_48" class="form-input-wide">
               <input type="number" id="input_48" name="maximum_budget" data-type="input-number" class=" form-number-input form-textbox" style="width:60px" size="5" value="" placeholder="ex: 23" data-component="number">
            </div>
         </li>
         <li class="form-line" data-type="control_textarea" id="id_12">
            <label class="form-label form-label-top" id="label_12" for="input_12"> Anything else to say? </label>
            <div id="cid_12" class="form-input-wide">
               <textarea id="input_12" class="form-textarea" name="extra_requirments" cols="60" rows="6" data-component="textarea"></textarea>
            </div>
         </li>
         <li class="form-line" data-type="control_widget" id="id_13" style="display: none;">
            <div id="cid_13" class="">
               <div style="width:100%;text-align:Left" data-component="widget-directEmbed">
                  <div class="direct-embed-widgets " data-type="direct-embed" style="width:1px;height:1px">
                     <div class="direct-embed-13">
                     </div>
                  </div>
               </div>
            </div>
         </li>
         <li class="form-line" data-type="control_checkbox" id="id_34">
            <label class="form-label form-label-top form-label-auto" id="label_34" for="input_34_0"> Authorized to contact me </label>
            <div id="cid_34" class="form-input-wide">
               <div class="form-single-column" data-component="checkbox">
                  <span class="form-checkbox-item" style="clear:left">
                  <span class="dragger-item">
                  </span>
                  <input type="checkbox" class="form-checkbox" id="input_34_0" name="authorized_to[]" value="A. Individual sellers,">
                  <label id="label_input_34_0" for="input_34_0"> A. Individual sellers, </label>
                  </span>
                  <span class="form-checkbox-item" style="clear:left">
                  <span class="dragger-item">
                  </span>
                  <input type="checkbox" class="form-checkbox" id="input_34_1" name="authorized_to[]" value="B. Agent-free of charge,">
                  <label id="label_input_34_1" for="input_34_1"> B. Agent-free of charge, </label>
                  </span>
                  <span class="form-checkbox-item" style="clear:left">
                  <span class="dragger-item">
                  </span>
                  <input type="checkbox" class="form-checkbox" id="input_34_2" name="authorized_to[]" value="C. An agent who charge commission,">
                  <label id="label_input_34_2" for="input_34_2"> C. An agent who charge commission, </label>
                  </span>
               </div>
            </div>
         </li>
         <li class="form-line jf-required" data-type="control_fullname" id="id_3">
            <label class="form-label form-label-top form-label-auto" id="label_3" for="first_3">
            Full Name
            <span class="form-required">
            *
            </span></label>
            <div id="cid_3" class="form-input-wide jf-required">
               <div data-wrapper-react="true">
                  <span class="form-sub-label-container" style="vertical-align:top">
                  <input type="text" id="first_3" name="full_name[first]" class="form-textbox validate[required]" size="10" value="" data-component="first" required="">
                  <label class="form-sub-label" for="first_3" id="sublabel_first" style="min-height:13px"> First Name </label>
                  </span>
                  <span class="form-sub-label-container" style="vertical-align:top">
                  <input type="text" id="last_3" name="full_name[last]" class="form-textbox validate[required]" size="15" value="" data-component="last" required="">
                  <label class="form-sub-label" for="last_3" id="sublabel_last" style="min-height:13px"> Last Name </label>
                  </span>
               </div>
            </div>
         </li>
         <li class="form-line jf-required" data-type="control_email" id="id_4">
            <label class="form-label form-label-top form-label-auto" id="label_4" for="input_4">
            E-mail
            <span class="form-required">
            *
            </span></label>
            <div id="cid_4" class="form-input-wide jf-required">
               <input type="email" id="input_4" name="user_email" class="form-textbox validate[required, Email]" size="30" value="" placeholder="ex: myname@example.com" data-component="email" required="">
            </div>
         </li>
         <li class="form-line jf-required" data-type="control_widget" id="id_28" data-progress="false">
            <label class="form-label form-label-top form-label-auto" id="label_28" for="input_28">
            Mobile number
            <span class="form-required">
            *
            </span></label>
            <div id="cid_28" class="form-input-wide jf-required">
               <div style="width:100%;text-align:Left" data-component="widget-field">
                  <input type="text" id="mobile_number" name="mobile_number" class="form-textbox validate[required]" size="15" value="" data-component="last" required="">
               </div>
            </div>
         </li>
         <li class="form-line" data-type="control_button" id="id_2">
            <div id="cid_2" class="form-input-wide">
               <div style="margin-left:156px" class="form-buttons-wrapper">
                  <button id="input_2" name="submit_request" type="submit" class="form-submit-button" data-component="button">
                  Submit Form
                  </button>
               </div>
            </div>
         </li>
      </ul>
   </div>
</form> 
  <?php
}
function request_listing_script_back_css() {

}
function rl_show_paid_listing() {
  //ob_start();

 
    // define query parameters based on attributes
    $options = array(
    'post_type' => 'estate_property',
        'posts_per_page' => 10,
        'order' => 'ASC',
        'orderby' => 'title',
    );

    $query = new WP_Query( $options );
    
    // run the loop based on the query
    if ( $query->have_posts() ) { ?>
         
       <?php while ( $query->have_posts() ) : $query->the_post(); 
       
       $post_id_get = get_the_ID(); 
       $title          =   get_the_title();
       $link           =   get_permalink();
       $preview        =   array();
       $preview[0]     =   '';
       $favorite_class =   'icon-fav-off';
       $fav_mes        =   __('add to favorites','wpestate');
       $pay_status = get_post_meta(get_the_ID(), 'pay_status', true);
       
       if($pay_status =='paid'){
       
       ?>

      <!-- Custom code html from theme -->
  <div class="col-md-3 listing_wrapper" data-org="<?php echo esc_html($col_org);?>" data-listid="<?php echo intval($post->ID);?>" > 
    <div class="property_listing  <?php if($wpestate_uset_unit==1) print 'property_listing_custom_design' ?> " data-link="<?php   if(  $property_unit_slider==0){ echo esc_url($link);}?>">
     

      <!-- status -->
      <?php
      
      if($pay_status=='paid'){
          $is_pay_status.='<span class="label label-success">'.__('Paid','wpestate').'</span>';
      }
      if($pay_status=='not paid'){
          $is_pay_status.='<span class="label label-info">'.__('Not Paid','wpestate').'</span>';
      }
      ?>
      <div style="position: absolute;top: 14px;left: 10px;z-index: 1;" class="user_dashboard_status">
            <?php print $is_pay_status;?>      
      </div>

        <?php if ($wpestate_uset_unit==1){
            if ( isset($show_remove_fav) && $show_remove_fav==1 ) {
                print '<span class="icon-fav icon-fav-on-remove" data-postid="'.$post->ID.'"> '.$fav_mes.'</span>';
            }
  
         wpestate_build_unit_custom_structure($custom_unit_structure,$post->ID,$property_unit_slider);
            
        } else{
            if ( has_post_thumbnail() ){
                $arguments      = array(
                                        'numberposts' => -1,
                                        'post_type' => 'attachment',
                                        'post_mime_type' => 'image',
                                        'post_parent' => $post->ID,
                                        'post_status' => null,
                                        'exclude' => get_post_thumbnail_id(),
                                        'orderby' => 'menu_order',
                                        'order' => 'ASC'
                                    );
                $post_attachments   = get_posts($arguments);
                
                    
                $pinterest = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_full_map');
                $preview   = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_listings');
                $compare   = wp_get_attachment_image_src(get_post_thumbnail_id(), 'slider_thumb');
                $extra= array(
                    'data-original' =>  $preview[0],
                    'class'         =>  'lazyload img-responsive',    
                );


                $thumb_prop             =   get_the_post_thumbnail($post->ID, 'property_listings',$extra);

                if($thumb_prop ==''){
                    $thumb_prop_default =  get_template_directory_uri().'/img/defaults/default_property_listings.jpg';
                    $thumb_prop         =  '<img src="'.$thumb_prop_default.'" class="b-lazy img-responsive wp-post-image  lazy-hidden" alt="no thumb" />';   
                }


                $prop_stat              =   esc_html( get_post_meta($post_id_get, 'property_status', true) );
                $featured               =   intval  ( get_post_meta($post_id_get, 'prop_featured', true) );
                $property_rooms         =   get_post_meta($post_id_get, 'property_bedrooms', true);
                if($property_rooms!=''){
                    $property_rooms=floatval($property_rooms);
                }

                $property_bathrooms     =   get_post_meta($post_id_get, 'property_bathrooms', true) ;
                if($property_bathrooms!=''){
                    $property_bathrooms=floatval($property_bathrooms);
                }

                $property_size          =   get_post_meta($post_id_get, 'property_size', true) ;
                if($property_size){
                    $property_size=wpestate_sizes_no_format(floatval($property_size));
                }




               $measure_sys = esc_html ( get_option('wp_estate_measure_sys','') ); 

                print   '<div class="listing-unit-img-wrapper">';
                print   '<div class="prop_new_details"><div class="prop_new_details_back"></div>';
            
                print '<div class="property_media">';
                    
                if( get_post_meta($post_id_get, 'embed_video_id', true)!='' ){
                    print '<i class="fa fa-video-camera" aria-hidden="true"></i>';
                }
                
                print'<i class="fa fa-camera" aria-hidden="true"></i> '.(count($post_attachments)+1).'
                </div>';
                    
                $property_city      =   get_the_term_list($post->ID, 'property_city', '', ', ', '') ;
                $property_area      =   get_the_term_list($post->ID, 'property_area', '', ', ', '');
                if( $property_city!='' || $property_area!='' ){
                    print '<div class="property_location_image"> 
                    <span class="property_marker"></span>';
                    if($property_area!=''){
                        print $property_area.', ';
                    }
                    if($property_city!=''){
                        print $property_city;
                    }
                    
                    print '</div>';
                }
                
                print '</div>';    
                
                if(  $property_unit_slider==1){
                    //slider
                   

                    $slides='';

                    $no_slides = 0;
                    foreach ($post_attachments as $attachment) { 
                        $no_slides++;
                        $preview    =   wp_get_attachment_image_src($attachment->ID, 'property_listings');

                        $slides     .= '<div class="item lazy-load-item">
                                            <a href="'.$link.'"><img  data-lazy-load-src="'.$preview[0].'" alt="'.$title.'" class="img-responsive" /></a>
                                        </div>';

                    }// end foreach
                    $unique_prop_id=uniqid();
                    print '
                    <div id="property_unit_carousel_'.$unique_prop_id.'" class="carousel property_unit_carousel slide " data-ride="carousel" data-interval="false">
                        <div class="carousel-inner">         
                            <div class="item active">    
                                <a href="'.$link.'">'.$thumb_prop.'</a>     
                            </div>
                            '.$slides.'
                        </div>




                        <a href="'.$link.'"> </a>';

                        if( $no_slides>0){
                            print '<a class="left  carousel-control" href="#property_unit_carousel_'.$unique_prop_id.'" data-slide="prev">
                                <i class="fa fa-angle-left"></i>
                            </a>

                            <a class="right  carousel-control" href="#property_unit_carousel_'.$unique_prop_id.'" data-slide="next">
                                <i class="fa fa-angle-right"></i>
                            </a>';
                        }
                    print'
                    </div>';


                }else{
                    print   '<a href="'.$link.'">'.$thumb_prop.'</a>';
                  
                }

                print   '</div>';

                print'<div class="tag-wrapper">';
                    if($featured==1){
                        print '<div class="featured_div">'.__('Featured','wpestate').'</div>';
                    }
                    if ($prop_stat != 'normal') {
                        $ribbon_class = str_replace(' ', '-', $prop_stat);
                        if (function_exists('icl_translate') ){
                            $prop_stat     =   icl_translate('wpestate','wp_estate_property_status'.$prop_stat, $prop_stat );
                        }
                          print'<div class="ribbon-wrapper-default ribbon-wrapper-' . $ribbon_class . '"><div class="ribbon-inside ' . $ribbon_class . '">' . $prop_stat . '</div></div>';
                    } 
                    
                print   '</div>';
            }
       
            if ( isset($show_remove_fav) && $show_remove_fav==1 ) {
                print '<span class="icon-fav icon-fav-on-remove" data-postid="'.$post->ID.'"> '.$fav_mes.'</span>';
            }
            ?>


            <h4><a href="<?php echo esc_url($link); ?>">
                <?php
                    echo mb_substr( $title,0,44); 
                    if(mb_strlen($title)>44){
                        echo '...';   
                    } 
                 
                ?>
                </a> 
            </h4> 
            
            <?php
            print '<div class="listing_unit_price_wrapper">';
                wpestate_show_price($post_id_get,$currency,$where_currency);
            print '</div>';
            ?>
        
        
        
           
        
            <?php if ($align_class=='the_list_view') {?>
                <div class="listing_details the_list_view" style="display:block;">
                    <?php   
                        echo wpestate_strip_excerpt_by_char(get_the_excerpt(),200,$post->ID);
                    ?>
                </div>   
        
                <div class="listing_details half_map_list_view" >
                    <?php   
                        echo wpestate_strip_excerpt_by_char(get_the_excerpt(),90,$post->ID);
                    ?>
                </div>   

                <div class="listing_details the_grid_view" style="display:none;">
                    <?php 
                        echo  wpestate_strip_excerpt_by_char(get_the_excerpt(),115,$post->ID);
                    ?>
                </div>
            <?php
            }else{
            ?>
                <div class="listing_details the_grid_view">
                    <?php
                        echo wpestate_strip_excerpt_by_char(get_the_excerpt(),115,$post->ID);
                    ?>
                </div>

                <div class="listing_details the_list_view">
                    <?php
                        echo  wpestate_strip_excerpt_by_char(get_the_excerpt(),115,$post->ID);
                    ?>
                </div>
            <?php } ?>   


            <div class="property_listing_details">
                <?php 
                    if($property_rooms!=''){
                        print ' <span class="inforoom">'.$property_rooms.'</span>';
                    }

                    if($property_bathrooms!=''){
                        print '<span class="infobath">'.$property_bathrooms.'</span>';
                    }

                    if($property_size!=''){
                        /* print ' <span class="infosize">'.$property_size.' '.$measure_sys.'<sup>2</sup></span>'; */
                         print ' <span class="infosize">'.$property_size.'</span>';
                    }
                    
                    echo '<a href="'.esc_url($link).'" class="unit_details_x">'.__('full info','wpestate').'</a>';
                  ?>
            </div>
               
            <div class="property_location">
                    
                    <?php 
                    $agent_id       =   intval  ( get_post_meta($post_id_get, 'property_agent', true) );
                    $thumb_id       =   get_post_thumbnail_id($agent_id);
                    $agent_face     =   wp_get_attachment_image_src($thumb_id, 'agent_picture_thumb');

                    if ($agent_face[0]==''){
                       $agent_face[0]= get_template_directory_uri().'/img/default-user_1.png';
                    }
                    
                    ?>
                    <div class="property_agent_wrapper">
                        <div class="property_agent_image" style="background-image:url('<?php echo $agent_face[0]; ?>')"></div> 
                        <div class="property_agent_image_sign"><i class="fa fa-user-circle-o" aria-hidden="true"></i></div>
                        <?php if($agent_id!=0){
                                echo '<a href="'.get_permalink($agent_id).'">'.get_the_title($agent_id).'</a>';
                            }else{
                                echo get_the_author_meta( 'nicename',$post->post_author);
                            }
                        ?>
                    </div>
                
                      <?php

                        if( !isset($show_compare) || $show_compare!=0  ){
                            $protocol = is_ssl() ? 'https' : 'http'; ?>
                            <div class="listing_actions">

                                <div class="share_unit">
                                    <a href="<?php echo $protocol;?>://www.facebook.com/sharer.php?u=<?php echo esc_url($link); ?>&amp;t=<?php echo urlencode(get_the_title()); ?>" target="_blank" class="social_facebook"></a>
                                    <a href="<?php echo $protocol;?>://twitter.com/home?status=<?php echo urlencode(get_the_title().' '.$link); ?>" class="social_tweet" target="_blank"></a>
                                    <a href="<?php echo $protocol;?>://plus.google.com/share?url=<?php echo esc_url($link); ?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" target="_blank" class="social_google"></a> 
                                    <a href="<?php echo $protocol;?>://pinterest.com/pin/create/button/?url=<?php echo esc_url($link); ?>&amp;media=<?php if (isset( $pinterest[0])){ echo esc_url($pinterest[0]); }?>&amp;description=<?php echo urlencode(get_the_title()); ?>" target="_blank" class="social_pinterest"></a>
                                </div>



                                <span class="share_list"  data-original-title="<?php _e('share','wpestate');?>" ></span>
                                <span class="icon-fav <?php echo esc_html($favorite_class);?>" data-original-title="<?php print $fav_mes; ?>" data-postid="<?php echo intval($post->ID); ?>"></span>

                                <?php if( $show_compare_only!='no') { ?>
                                <span class="compare-action" data-original-title="<?php  _e('compare','wpestate');?>" data-pimage="<?php if( isset($compare[0])){echo esc_html($compare[0]);} ?>" data-pid="<?php echo intval($post->ID); ?>"></span>
                                <?php } ?>
                            </div>
                        <?php
                        } 

                print '</div>';        

               
              
            }// end if custom structure
            ?>
        </div>             
    </div>
      <!-- Custom code html from theme Ends -->

<?php
  }


//Unset Values
unset($is_pay_status);
unset($pinterest);
unset($previe);
unset($compare);
unset($extra);
unset($property_size);
unset($property_bathrooms);
unset($property_rooms);
unset($measure_sys);
unset($col_class);
unset($col_org);
unset($link);
unset($preview);
unset($favorite_class);
unset($fav_mes);
unset($curent_fav);
unset($thumb_prop);
unset($prop_stat);
unset($featured);
unset($property_rooms);
unset($property_bathrooms);
unset($property_size);
unset($prop_stat);
unset($ribbon_class);
unset($property_city);
unset($property_area);
unset($show_remove_fav);
unset($title);
unset($align_class);
?>
    <?php endwhile;
    wp_reset_postdata(); ?>



    <?php
        //$myvariable = ob_get_clean();
        //return $myvariable;
    }





}





function request_listing_script_front_css() {
    /* CSS */
        wp_register_style('request_listingstyle', plugins_url('css/request_listing.css',__FILE__));
        wp_enqueue_style('request_listingstyle');
}

    add_action( 'wp_ajax_my_ajax_rt', 'my_ajax_rt' );
    add_action( 'wp_ajax_nopriv_my_ajax_rt', 'my_ajax_rt' );
function my_ajax_rt() {
  
}

function request_listing_script_back_js() {
  
}



function request_listing_script_front_js() {
  
         
        wp_register_script('request_listing_script', plugins_url('js/request_listing.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('request_listing_script');

}
