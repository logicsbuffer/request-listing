<?php
// Template Name: Advertise With
// Wp Estate Pack
get_header();
wp_suspend_cache_addition(true);
$options=wpestate_page_details($post->ID);
global $no_listins_per_row;
$no_listins_per_row       =   intval( get_option('wp_estate_agent_listings_per_row', '') );

$col_class=4;
if($options['content_class']=='col-md-12'){
    $col_class=3;
}

if($no_listins_per_row==3){
    $col_class  =   '6';
    $col_org    =   6;
    if($options['content_class']=='col-md-12'){
        $col_class  =   '4';
        $col_org    =   4;
    }
}else{   
    $col_class  =   '4';
    $col_org    =   4;
    if($options['content_class']=='col-md-12'){
        $col_class  =   '3';
        $col_org    =   3;
    }
}
//if( 'POST' == $_SERVER['REQUEST_METHOD'] && $_POST['action']=='submit_ad' ) {
	if(isset($_POST['submit_ad'])){
?><pre><?php print_r($_POST); ?> </pre><?php
    $paid_submission_status    = esc_html ( get_option('wp_estate_paid_submission','') );
     
    if ( $paid_submission_status!='membership' || ( $paid_submission_status== 'membership' || wpestate_get_current_user_listings($userID) > 0)  ){ // if user can submit
        
        if ( !isset($_POST['new_estate']) || !wp_verify_nonce($_POST['new_estate'],'submit_new_estate') ){
           exit('Sorry, your not submiting from site'); 
        }
   
        if( !isset($_POST['prop_category']) ) {
            $prop_category=0;           
        }else{
            $prop_category  =   intval($_POST['prop_category']);
        }
  
        if( !isset($_POST['prop_action_category']) ) {
            $prop_action_category=0;           
        }else{
            $prop_action_category  =   wp_kses(esc_html($_POST['prop_action_category']),$allowed_html);
        }
        
        if( !isset($_POST['property_city']) ) {
            $property_city='';           
        }else{
            $property_city  =   wp_kses(esc_html($_POST['property_city']),$allowed_html);
        }
        
        if( !isset($_POST['property_area']) ) {
            $property_area='';           
        }else{
            $property_area  =   wp_kses(esc_html($_POST['property_area']),$allowed_html);
        }
       
        
        if( !isset($_POST['property_county']) ) {
            $property_county_state='';           
        }else{
            $property_county_state  =   wp_kses(esc_html($_POST['property_county']),$allowed_html);
        }
       
        
        
        
                
        $show_err                       =   '';
        $post_id                        =   '';
        $submit_title                   =   wp_kses( $_POST['wpestate_title'],$allowed_html ); 
        $submit_description             =   wp_filter_nohtml_kses( $_POST['wpestate_description']);     
        $property_address               =   wp_kses( $_POST['property_address'],$allowed_html);
        $property_county                =   wp_kses( $_POST['property_county'],$allowed_html);
        $property_zip                   =   wp_kses( $_POST['property_zip'],$allowed_html);
        $country_selected               =   wp_kses( $_POST['property_country'],$allowed_html);     
        $prop_stat                      =   wp_kses( $_POST['property_status'],$allowed_html);
        $property_status                =   '';
        
        foreach ($status_values_array as $key=>$value) {
            $value = trim($value);
            $value_wpml=$value;
            $slug_status=sanitize_title($value);
            if (function_exists('icl_translate') ){
                $value_wpml= icl_translate('wpestate','wp_estate_property_status_front_'.$slug_status,$value );
            }
            
            $property_status.='<option value="' . $value . '"';
            if ($value == $prop_stat) {
                $property_status.='selected="selected"';
            }
            $property_status.='>' . $value_wpml . '</option>';
        }

        $property_price                 =   wp_kses( esc_html($_POST['property_price']),$allowed_html);
        $property_label                 =   wp_kses( esc_html($_POST['property_label']),$allowed_html);   
        $property_label_before          =   wp_kses( esc_html($_POST['property_label_before']),$allowed_html); 
        $property_size                  =   wp_kses( esc_html($_POST['property_size']),$allowed_html);  
        $owner_notes                    =   wp_kses( esc_html($_POST['owner_notes']),$allowed_html);  
        $property_lot_size              =   wp_kses( esc_html($_POST['property_lot_size']),$allowed_html); 
        $property_rooms                 =   wp_kses( esc_html($_POST['property_rooms']),$allowed_html); 
        $property_bedrooms              =   wp_kses( esc_html($_POST['property_bedrooms']),$allowed_html); 
        $property_bathrooms             =   wp_kses( esc_html($_POST['property_bathrooms']),$allowed_html); 
        $option_video                   =   '';
        $video_values                   =   array('vimeo', 'youtube');
        $video_type                     =   wp_kses( esc_html($_POST['embed_video_type']),$allowed_html); 
        $google_camera_angle            =   wp_kses( esc_html($_POST['google_camera_angle']),$allowed_html); 
        $property_has_subunits          =   wp_kses( esc_html($_POST['property_has_subunits']),$allowed_html); 
        if(isset($_POST['property_subunits_list'])){
            $property_subunits_list         =   $_POST['property_subunits_list']; 
        }
        $has_errors                      =   false;
        
        
       
        
        $errors                         =   array();
        
        $moving_array=array();
        foreach($feature_list_array as $key => $value){
            $post_var_name    =   str_replace(' ','_', trim($value) );
            $post_var_name    =   wpestate_limit45(sanitize_title( $post_var_name ));
            $post_var_name    =   sanitize_key($post_var_name);
            
            if(isset($_POST[$post_var_name])){
                $feature_value    =   wp_kses( esc_html($_POST[$post_var_name]) ,$allowed_html);
            }
            
            if($feature_value==1){
                $moving_array[]=$post_var_name;
            }        
       }
        
      
        foreach ($video_values as $value) {
            $option_video.='<option value="' . $value . '"';
            if ($value == $video_type) {
                $option_video.='selected="selected"';
            }
            $option_video.='>' . $value . '</option>';
        }
        
        $option_slider='';
        $slider_values = array('full top slider', 'small slider'); 
        $iframe = array( 'iframe' => array(
                         'src' => array (),
                         'width' => array (),
                         'height' => array (),
                         'frameborder' => array(),
                        'style' => array(),
                         'allowFullScreen' => array() // add any other attributes you wish to allow
                          ) );
      
        $embed_video_id                 =   wp_kses( esc_html($_POST['embed_video_id']),$allowed_html); 
        $virtual_tour                   =   wp_kses (trim($_POST['embed_virtual_tour']),$iframe) ;
        $property_latitude              =   floatval( $_POST['property_latitude']); 
        $property_longitude             =   floatval( $_POST['property_longitude']); 
        $google_view                    =   wp_kses( esc_html( $_POST['property_google_view']),$allowed_html); 

        if($google_view==1){
            $google_view_check=' checked="checked" ';
        }else{
            $google_view_check=' ';
        }
   
        $google_camera_angle            =   intval( $_POST['google_camera_angle']); 
        $prop_category                  =   get_term( $prop_category, 'property_category');
        if(isset($prop_category->term_id)){
            $prop_category_selected         =   $prop_category->term_id;
        }
    
        $prop_action_category           =   get_term( $prop_action_category, 'property_action_category');  
        if(isset($prop_action_category->term_id)){
             $prop_action_category_selected  =   $prop_action_category->term_id;
        }
       
        
        // save custom fields
     
        $i=0;
        if( !empty($custom_fields)){  
            while($i< count($custom_fields) ){
               $name    =   $custom_fields[$i][0];
               $type    =   $custom_fields[$i][1];
               $slug    =   str_replace(' ','_',$name);
               $slug    =   wpestate_limit45(sanitize_title( $name ));
               $slug    =   sanitize_key($slug);
               $custom_fields_array[$slug]= wp_kses( esc_html($_POST[$slug]),$allowed_html);
               $i++;
            }
        }    
            
        if($submit_title==''){
            $has_errors=true;
            //print'<div class="col-md-12 row_dasboard-prop-listing">';
            $errors[]=__('Please submit a title for your property request','wpestate');
            //print'</div>';
        }

        if($submit_description==''){
            $has_errors=true;
           // print'<div class="col-md-12 row_dasboard-prop-listing">';
            $errors[]=__('Please submit a description for your property request','wpestate');
            //print'</div>';
        }
        
        /* if ($_POST['attachid']==''){
            $has_errors=true;
           // print'<div class="col-md-12 row_dasboard-prop-listing">';
            $errors[]=__('Please submit an image for your property','wpestate'); 
            //print'</div>';
        } */
        
        if($property_address==''){
            $has_errors=true;
            //print'<div class="col-md-12 row_dasboard-prop-listing">';
            $errors[]=__('Please submit an address for your property request','wpestate');
            //print'<div>';
        }

        if($has_errors){
            foreach($errors as $key=>$value){
                $show_err.=$value.'</br>';
            }            
        }else{
            $paid_submission_status = esc_html ( get_option('wp_estate_paid_submission','') );
            $new_status             = 'pending';
            
            $admin_submission_status= esc_html ( get_option('wp_estate_admin_submission','') );
            if($admin_submission_status=='no' && $paid_submission_status!='per listing'){
               $new_status='publish';  
            }
            
            
            $post = array(
                'post_title'	=> $submit_title,
                'post_content'	=> $submit_description,
                'post_status'	=> $new_status, 
                'post_type'     => 'estate_property' ,
                'post_author'   => $current_user->ID 
            );
            $post_id =  wp_insert_post($post );  
            
            if( $paid_submission_status == 'membership'){ // update pack status
                wpestate_update_listing_no($current_user->ID);                
            }
       
        }
        
      

        
        
        if($post_id) {
            // uploaded images
            $order=0;
            /* $attchs=explode(',',$_POST['attachid']);
            $last_id='';
            foreach($attchs as $att_id){
                if( !is_numeric($att_id) ){
                 
                }else{
                    if($last_id==''){
                        $last_id=  $att_id;  
                    }
                    $order++;
                    wp_update_post( array(
                                'ID' => $att_id,
                                'post_parent' => $post_id,
                                'menu_order'=>$order
                            ));
                        
                    
                }
            }
            
            if( is_numeric($_POST['attachthumb']) && $_POST['attachthumb']!=''  ){
                set_post_thumbnail( $post_id, wp_kses(esc_html($_POST['attachthumb']),$allowed_html )); 
            }else{
                set_post_thumbnail( $post_id, $last_id );                
            } */
            //end uploaded images
            
            
            if( isset($prop_category->name) ){
                wp_set_object_terms($post_id,$prop_category->name,'property_category'); 
            }  
            if ( isset ($prop_action_category->name) ){
                wp_set_object_terms($post_id,$prop_action_category->name,'property_action_category'); 
            }  
            if( isset($property_city) && $property_city!='none' ){
                if($property_city == -1 ){
                    $property_city='';
                }
                
                wp_set_object_terms($post_id,$property_city,'property_city'); 
            }  
          
            if( isset($property_area) && $property_area!='none' ){
                wp_set_object_terms($post_id,$property_area,'property_area'); 
            }  
            
            if( isset($property_county_state) && $property_county_state!='none' ){
                if($property_county_state == -1){
                    $property_county_state='';
                }
                wp_set_object_terms($post_id,$property_county_state,'property_county_state'); 
            }  
            
            if($property_area!=''){
                $terms= get_term_by('name', $property_area, 'property_area');
                if($terms!=''){
                    $t_id=$terms->term_id;
                    $term_meta=array('cityparent'=>$property_city);
                    add_option( "taxonomy_$t_id", $term_meta ); 
                }
                
            }
          
   
            update_post_meta($post_id, 'sidebar_agent_option', 'global');
            update_post_meta($post_id, 'local_pgpr_slider_type', 'global');
            update_post_meta($post_id, 'local_pgpr_content_type', 'global');
            update_post_meta($post_id, 'prop_featured', 0);
            update_post_meta($post_id, 'property_address', $property_address);
            update_post_meta($post_id, 'property_county', $property_county);
            update_post_meta($post_id, 'property_zip', $property_zip);
            update_post_meta($post_id, 'property_country', $country_selected);
            update_post_meta($post_id, 'property_size', $property_size);
            update_post_meta($post_id, 'owner_notes', $owner_notes);
            update_post_meta($post_id, 'property_lot_size', $property_lot_size);  
            update_post_meta($post_id, 'property_rooms', $property_rooms);  
            update_post_meta($post_id, 'property_has_subunits', $property_has_subunits);  
            update_post_meta($post_id, 'property_subunits_list', $property_subunits_list); 
            
            if(is_array($property_subunits_list)){
            foreach ($property_subunits_list as $key) {
                update_post_meta(intval($key), 'property_subunits_master',$post_id );
            }
            }else{
                update_post_meta(intval($property_subunits_list), 'property_subunits_master',$post_id );
            }
        
            
            
            update_post_meta($post_id, 'property_bedrooms', $property_bedrooms);
            update_post_meta($post_id, 'property_bathrooms', $property_bathrooms);
            update_post_meta($post_id, 'property_status', $prop_stat);
            update_post_meta($post_id, 'property_price', $property_price);
            update_post_meta($post_id, 'property_label', $property_label);
            update_post_meta($post_id, 'property_label_before', $property_label_before);
            update_post_meta($post_id, 'embed_video_type', $video_type);
            update_post_meta($post_id, 'embed_video_id',  $embed_video_id );
            update_post_meta($post_id,  'virtual_tour', $virtual_tour);                 
            update_post_meta($post_id, 'property_latitude', $property_latitude);
            update_post_meta($post_id, 'property_longitude', $property_longitude);
            update_post_meta($post_id, 'property_google_view',  $google_view);
            update_post_meta($post_id, 'google_camera_angle', $google_camera_angle);
            update_post_meta($post_id, 'pay_status', 'not paid');
            update_post_meta($post_id, 'page_custom_zoom', 16);
            
            if('yes' ==  esc_html ( get_option('wp_estate_user_agent','') )){
                $user_id_agent            =   get_the_author_meta( 'user_agent_id' , $current_user->ID  );
                update_post_meta($post_id, 'property_agent', $user_id_agent);                
            }
           
            // save custom fields
            $custom_fields = get_option( 'wp_estate_custom_fields', true);  
     
            $i=0;
            if( !empty($custom_fields)){  
                while($i< count($custom_fields) ){
                    $name   =   $custom_fields[$i][0];
                    $type   =   $custom_fields[$i][2];
                    $slug   =   str_replace(' ','_',$name);
                    $slug   =   wpestate_limit45(sanitize_title( $name ));
                    $slug   =   sanitize_key($slug);
                   if($type=='numeric'){
                        $value_custom    =   intval(wp_kses( $_POST[$slug],$allowed_html ) );
                       
                        if($value_custom==0){
                            $value_custom='';
                        }
                       
                       update_post_meta($post_id, $slug, $value_custom);
                   }else{
                       $value_custom    =   esc_html(wp_kses( $_POST[$slug],$allowed_html ) );
                       update_post_meta($post_id, $slug, $value_custom);
                   }
                   $custom_fields_array[$slug]= wp_kses( esc_html($_POST[$slug]),$allowed_html);
                   $i++;
                }
            }
            
            
            
            
            foreach($feature_list_array as $key => $value){
                $post_var_name      =   str_replace(' ','_', trim($value) );
                $post_var_name      =   wpestate_limit45(sanitize_title( $post_var_name ));
                $post_var_name      =   sanitize_key($post_var_name);
                
                if( isset($_POST[$post_var_name])){
                    $feature_value  =   wp_kses( esc_html($_POST[$post_var_name]) ,$allowed_html);
                    update_post_meta($post_id, $post_var_name, $feature_value);
                }               
                $moving_array[] =   $post_var_name;
            }
   
            // get user dashboard link
            $redirect = get_dashboard_link();
            
            $arguments=array(
                'new_listing_url'   => get_permalink($post_id),
                'new_listing_title' => $submit_title
            );
            wpestate_select_email_type(get_option('admin_email'),'new_listing_submission',$arguments);
    
            wp_reset_query();
            wp_redirect( $redirect);
            exit;
        }
        
        }//end if user can submit   
} // end post



?>

	<div class="row">
    <?php get_template_part('templates/breadcrumbs'); ?>
    <div class=" <?php print esc_html($options['content_class']);?> ">
    <?php get_template_part('templates/ajax_container'); ?>

 <form action="" method="post">
 <input type="hidden" name="temp_upload_folder" value="81093335742456_5b0c354c7e378">
  <input type="hidden" name="formID" value="81093335742456">
  <div class="form-all">
    <ul class="form-section page-section">
      <li id="cid_1" class="form-input-wide" data-type="control_head">
        <div class="form-header-group ">
          <div class="header-text httac htvam">
            <h2 id="header_1" class="form-header" data-component="header">
              4 Advertising for real estate agents and businesses
            </h2>
          </div>
        </div>
      </li>
      <li class="form-line" data-type="control_widget" id="id_13">
        <label class="form-label form-label-top" id="label_13" for="input_13"> Add your logo (optional) </label>
        <div id="cid_13" class="form-input-wide">
        </div>
      </li>
      <li class="form-line jf-required" data-type="control_fullname" id="id_3">
        <label class="form-label form-label-top form-label-auto" id="label_3" for="first_3">
          Name
          <span class="form-required">
            *
          </span>
        </label>
        <div id="cid_3" class="form-input-wide jf-required">
          <div data-wrapper-react="true">
            <span class="form-sub-label-container" style="vertical-align:top">
              <input type="text" id="first_3" name="q3_name[first]" class="form-textbox validate[required]" size="10" value="" data-component="first" required="">
              <label class="form-sub-label" for="first_3" id="sublabel_first" style="min-height:13px"> First Name </label>
            </span>
            <span class="form-sub-label-container" style="vertical-align:top">
              <input type="text" id="last_3" name="q3_name[last]" class="form-textbox validate[required]" size="15" value="" data-component="last" required="">
              <label class="form-sub-label" for="last_3" id="sublabel_last" style="min-height:13px"> Last Name </label>
            </span>
          </div>
        </div>
      </li>
      <li class="form-line jf-required" data-type="control_email" id="id_4">
        <label class="form-label form-label-top form-label-auto" id="label_4" for="input_4">
          Email
          <span class="form-required">
            *
          </span>
        </label>
        <div id="cid_4" class="form-input-wide jf-required">
          <span class="form-sub-label-container" style="vertical-align:top">
            <input type="email" id="input_4" name="q4_email" class="form-textbox validate[required, Email]" size="30" value="" data-component="email" required="">
            <label class="form-sub-label" for="input_4" style="min-height:13px"> example@example.com </label>
          </span>
        </div>
      </li>
      <li class="form-line jf-required" data-type="control_address" id="id_5">
        <label class="form-label form-label-top form-label-auto" id="label_5" for="input_5_addr_line1">
          Address
          <span class="form-required">
            *
          </span>
        </label>
        <div id="cid_5" class="form-input-wide jf-required">
          <table summary="" class="form-address-table" border="0" cellpadding="0" cellspacing="0">
            <tbody>
              <tr>
                <td colspan="2">
                  <span class="form-sub-label-container" style="vertical-align:top">
                    <input type="text" id="input_5_addr_line1" name="q5_address[addr_line1]" class="form-textbox validate[required] form-address-line" value="" data-component="address_line_1" required="">
                    <label class="form-sub-label" for="input_5_addr_line1" id="sublabel_5_addr_line1" style="min-height:13px"> Street Address </label>
                  </span>
                </td>
              </tr>
              <tr>
                <td colspan="2">
                  <span class="form-sub-label-container" style="vertical-align:top">
                    <input type="text" id="input_5_addr_line2" name="q5_address[addr_line2]" class="form-textbox form-address-line" size="46" value="" data-component="address_line_2" required="">
                    <label class="form-sub-label" for="input_5_addr_line2" id="sublabel_5_addr_line2" style="min-height:13px"> Street Address Line 2 </label>
                  </span>
                </td>
              </tr>
              <tr>
                <td width="50%">
                  <span class="form-sub-label-container" style="vertical-align:top">
                    <input type="text" id="input_5_city" name="q5_address[city]" class="form-textbox validate[required] form-address-city" size="21" value="" data-component="city" required="">
                    <label class="form-sub-label" for="input_5_city" id="sublabel_5_city" style="min-height:13px"> City </label>
                  </span>
                </td>
                <td>
                  <span class="form-sub-label-container" style="vertical-align:top">
                    <input type="text" id="input_5_state" name="q5_address[state]" class="form-textbox validate[required] form-address-state" size="22" value="" data-component="state" required="">
                    <label class="form-sub-label" for="input_5_state" id="sublabel_5_state" style="min-height:13px"> State / Province </label>
                  </span>
                </td>
              </tr>
              <tr>
                <td width="50%">
                  <span class="form-sub-label-container" style="vertical-align:top">
                    <input type="text" id="input_5_postal" name="q5_address[postal]" class="form-textbox form-address-postal" size="10" value="" data-component="zip" required="">
                    <label class="form-sub-label" for="input_5_postal" id="sublabel_5_postal" style="min-height:13px"> Postal / Zip Code </label>
                  </span>
                </td>
                <td style="display:none">
                  <span class="form-sub-label-container" style="vertical-align:top">
                    <select class="form-dropdown validate[required] form-address-country" name="q5_address[country]" id="input_5_country" data-component="country" required="">
                      <option value=""> Please Select </option>
                      <option value="United States"> United States </option>
                      <option value="Afghanistan"> Afghanistan </option>
                      <option value="Albania"> Albania </option>
                      <option value="Algeria"> Algeria </option>
                      <option value="American Samoa"> American Samoa </option>
                      <option value="Andorra"> Andorra </option>
                      <option value="Angola"> Angola </option>
                      <option value="Anguilla"> Anguilla </option>
                      <option value="Antigua and Barbuda"> Antigua and Barbuda </option>
                      <option value="Argentina"> Argentina </option>
                      <option value="Armenia"> Armenia </option>
                      <option value="Aruba"> Aruba </option>
                      <option value="Australia"> Australia </option>
                      <option value="Austria"> Austria </option>
                      <option value="Azerbaijan"> Azerbaijan </option>
                      <option value="The Bahamas"> The Bahamas </option>
                      <option value="Bahrain"> Bahrain </option>
                      <option value="Bangladesh"> Bangladesh </option>
                      <option value="Barbados"> Barbados </option>
                      <option value="Belarus"> Belarus </option>
                      <option value="Belgium"> Belgium </option>
                      <option value="Belize"> Belize </option>
                      <option value="Benin"> Benin </option>
                      <option value="Bermuda"> Bermuda </option>
                      <option value="Bhutan"> Bhutan </option>
                      <option value="Bolivia"> Bolivia </option>
                      <option value="Bosnia and Herzegovina"> Bosnia and Herzegovina </option>
                      <option value="Botswana"> Botswana </option>
                      <option value="Brazil"> Brazil </option>
                      <option value="Brunei"> Brunei </option>
                      <option value="Bulgaria"> Bulgaria </option>
                      <option value="Burkina Faso"> Burkina Faso </option>
                      <option value="Burundi"> Burundi </option>
                      <option value="Cambodia"> Cambodia </option>
                      <option value="Cameroon"> Cameroon </option>
                      <option value="Canada"> Canada </option>
                      <option value="Cape Verde"> Cape Verde </option>
                      <option value="Cayman Islands"> Cayman Islands </option>
                      <option value="Central African Republic"> Central African Republic </option>
                      <option value="Chad"> Chad </option>
                      <option value="Chile"> Chile </option>
                      <option value="China"> China </option>
                      <option value="Christmas Island"> Christmas Island </option>
                      <option value="Cocos (Keeling) Islands"> Cocos (Keeling) Islands </option>
                      <option value="Colombia"> Colombia </option>
                      <option value="Comoros"> Comoros </option>
                      <option value="Congo"> Congo </option>
                      <option value="Cook Islands"> Cook Islands </option>
                      <option value="Costa Rica"> Costa Rica </option>
                      <option value="Cote d'Ivoire"> Cote d'Ivoire </option>
                      <option value="Croatia"> Croatia </option>
                      <option value="Cuba"> Cuba </option>
                      <option value="Cyprus"> Cyprus </option>
                      <option value="Czech Republic"> Czech Republic </option>
                      <option value="Democratic Republic of the Congo"> Democratic Republic of the Congo </option>
                      <option value="Denmark"> Denmark </option>
                      <option value="Djibouti"> Djibouti </option>
                      <option value="Dominica"> Dominica </option>
                      <option value="Dominican Republic"> Dominican Republic </option>
                      <option value="Ecuador"> Ecuador </option>
                      <option value="Egypt"> Egypt </option>
                      <option value="El Salvador"> El Salvador </option>
                      <option value="Equatorial Guinea"> Equatorial Guinea </option>
                      <option value="Eritrea"> Eritrea </option>
                      <option value="Estonia"> Estonia </option>
                      <option value="Ethiopia"> Ethiopia </option>
                      <option value="Falkland Islands"> Falkland Islands </option>
                      <option value="Faroe Islands"> Faroe Islands </option>
                      <option value="Fiji"> Fiji </option>
                      <option value="Finland"> Finland </option>
                      <option value="France"> France </option>
                      <option value="French Polynesia"> French Polynesia </option>
                      <option value="Gabon"> Gabon </option>
                      <option value="The Gambia"> The Gambia </option>
                      <option value="Georgia"> Georgia </option>
                      <option value="Germany"> Germany </option>
                      <option value="Ghana"> Ghana </option>
                      <option value="Gibraltar"> Gibraltar </option>
                      <option value="Greece"> Greece </option>
                      <option value="Greenland"> Greenland </option>
                      <option value="Grenada"> Grenada </option>
                      <option value="Guadeloupe"> Guadeloupe </option>
                      <option value="Guam"> Guam </option>
                      <option value="Guatemala"> Guatemala </option>
                      <option value="Guernsey"> Guernsey </option>
                      <option value="Guinea"> Guinea </option>
                      <option value="Guinea-Bissau"> Guinea-Bissau </option>
                      <option value="Guyana"> Guyana </option>
                      <option value="Haiti"> Haiti </option>
                      <option value="Honduras"> Honduras </option>
                      <option value="Hong Kong"> Hong Kong </option>
                      <option value="Hungary"> Hungary </option>
                      <option value="Iceland"> Iceland </option>
                      <option value="India"> India </option>
                      <option value="Indonesia"> Indonesia </option>
                      <option value="Iran"> Iran </option>
                      <option value="Iraq"> Iraq </option>
                      <option value="Ireland"> Ireland </option>
                      <option value="Israel"> Israel </option>
                      <option value="Italy"> Italy </option>
                      <option value="Jamaica"> Jamaica </option>
                      <option value="Japan"> Japan </option>
                      <option value="Jersey"> Jersey </option>
                      <option value="Jordan"> Jordan </option>
                      <option value="Kazakhstan"> Kazakhstan </option>
                      <option value="Kenya"> Kenya </option>
                      <option value="Kiribati"> Kiribati </option>
                      <option value="North Korea"> North Korea </option>
                      <option value="South Korea"> South Korea </option>
                      <option value="Kosovo"> Kosovo </option>
                      <option value="Kuwait"> Kuwait </option>
                      <option value="Kyrgyzstan"> Kyrgyzstan </option>
                      <option value="Laos"> Laos </option>
                      <option value="Latvia"> Latvia </option>
                      <option value="Lebanon"> Lebanon </option>
                      <option value="Lesotho"> Lesotho </option>
                      <option value="Liberia"> Liberia </option>
                      <option value="Libya"> Libya </option>
                      <option value="Liechtenstein"> Liechtenstein </option>
                      <option value="Lithuania"> Lithuania </option>
                      <option value="Luxembourg"> Luxembourg </option>
                      <option value="Macau"> Macau </option>
                      <option value="Macedonia"> Macedonia </option>
                      <option value="Madagascar"> Madagascar </option>
                      <option value="Malawi"> Malawi </option>
                      <option value="Malaysia"> Malaysia </option>
                      <option value="Maldives"> Maldives </option>
                      <option value="Mali"> Mali </option>
                      <option value="Malta"> Malta </option>
                      <option value="Marshall Islands"> Marshall Islands </option>
                      <option value="Martinique"> Martinique </option>
                      <option value="Mauritania"> Mauritania </option>
                      <option value="Mauritius"> Mauritius </option>
                      <option value="Mayotte"> Mayotte </option>
                      <option value="Mexico"> Mexico </option>
                      <option value="Micronesia"> Micronesia </option>
                      <option value="Moldova"> Moldova </option>
                      <option value="Monaco"> Monaco </option>
                      <option value="Mongolia"> Mongolia </option>
                      <option value="Montenegro"> Montenegro </option>
                      <option value="Montserrat"> Montserrat </option>
                      <option value="Morocco"> Morocco </option>
                      <option value="Mozambique"> Mozambique </option>
                      <option value="Myanmar"> Myanmar </option>
                      <option value="Nagorno-Karabakh"> Nagorno-Karabakh </option>
                      <option value="Namibia"> Namibia </option>
                      <option value="Nauru"> Nauru </option>
                      <option value="Nepal"> Nepal </option>
                      <option value="Netherlands"> Netherlands </option>
                      <option value="Netherlands Antilles"> Netherlands Antilles </option>
                      <option value="New Caledonia"> New Caledonia </option>
                      <option value="New Zealand"> New Zealand </option>
                      <option value="Nicaragua"> Nicaragua </option>
                      <option value="Niger"> Niger </option>
                      <option value="Nigeria"> Nigeria </option>
                      <option value="Niue"> Niue </option>
                      <option value="Norfolk Island"> Norfolk Island </option>
                      <option value="Turkish Republic of Northern Cyprus"> Turkish Republic of Northern Cyprus </option>
                      <option value="Northern Mariana"> Northern Mariana </option>
                      <option value="Norway"> Norway </option>
                      <option value="Oman"> Oman </option>
                      <option value="Pakistan"> Pakistan </option>
                      <option value="Palau"> Palau </option>
                      <option value="Palestine"> Palestine </option>
                      <option value="Panama"> Panama </option>
                      <option value="Papua New Guinea"> Papua New Guinea </option>
                      <option value="Paraguay"> Paraguay </option>
                      <option value="Peru"> Peru </option>
                      <option value="Philippines"> Philippines </option>
                      <option value="Pitcairn Islands"> Pitcairn Islands </option>
                      <option value="Poland"> Poland </option>
                      <option value="Portugal"> Portugal </option>
                      <option value="Puerto Rico"> Puerto Rico </option>
                      <option value="Qatar"> Qatar </option>
                      <option value="Republic of the Congo"> Republic of the Congo </option>
                      <option value="Romania"> Romania </option>
                      <option value="Russia"> Russia </option>
                      <option value="Rwanda"> Rwanda </option>
                      <option value="Saint Barthelemy"> Saint Barthelemy </option>
                      <option value="Saint Helena"> Saint Helena </option>
                      <option value="Saint Kitts and Nevis"> Saint Kitts and Nevis </option>
                      <option value="Saint Lucia"> Saint Lucia </option>
                      <option value="Saint Martin"> Saint Martin </option>
                      <option value="Saint Pierre and Miquelon"> Saint Pierre and Miquelon </option>
                      <option value="Saint Vincent and the Grenadines"> Saint Vincent and the Grenadines </option>
                      <option value="Samoa"> Samoa </option>
                      <option value="San Marino"> San Marino </option>
                      <option value="Sao Tome and Principe"> Sao Tome and Principe </option>
                      <option value="Saudi Arabia"> Saudi Arabia </option>
                      <option value="Senegal"> Senegal </option>
                      <option value="Serbia"> Serbia </option>
                      <option value="Seychelles"> Seychelles </option>
                      <option value="Sierra Leone"> Sierra Leone </option>
                      <option value="Singapore"> Singapore </option>
                      <option value="Slovakia"> Slovakia </option>
                      <option value="Slovenia"> Slovenia </option>
                      <option value="Solomon Islands"> Solomon Islands </option>
                      <option value="Somalia"> Somalia </option>
                      <option value="Somaliland"> Somaliland </option>
                      <option value="South Africa"> South Africa </option>
                      <option value="South Ossetia"> South Ossetia </option>
                      <option value="South Sudan"> South Sudan </option>
                      <option value="Spain"> Spain </option>
                      <option value="Sri Lanka"> Sri Lanka </option>
                      <option value="Sudan"> Sudan </option>
                      <option value="Suriname"> Suriname </option>
                      <option value="Svalbard"> Svalbard </option>
                      <option value="Swaziland"> Swaziland </option>
                      <option value="Sweden"> Sweden </option>
                      <option value="Switzerland"> Switzerland </option>
                      <option value="Syria"> Syria </option>
                      <option value="Taiwan"> Taiwan </option>
                      <option value="Tajikistan"> Tajikistan </option>
                      <option value="Tanzania"> Tanzania </option>
                      <option value="Thailand"> Thailand </option>
                      <option value="Timor-Leste"> Timor-Leste </option>
                      <option value="Togo"> Togo </option>
                      <option value="Tokelau"> Tokelau </option>
                      <option value="Tonga"> Tonga </option>
                      <option value="Transnistria Pridnestrovie"> Transnistria Pridnestrovie </option>
                      <option value="Trinidad and Tobago"> Trinidad and Tobago </option>
                      <option value="Tristan da Cunha"> Tristan da Cunha </option>
                      <option value="Tunisia"> Tunisia </option>
                      <option value="Turkey"> Turkey </option>
                      <option value="Turkmenistan"> Turkmenistan </option>
                      <option value="Turks and Caicos Islands"> Turks and Caicos Islands </option>
                      <option value="Tuvalu"> Tuvalu </option>
                      <option value="Uganda"> Uganda </option>
                      <option value="Ukraine"> Ukraine </option>
                      <option value="United Arab Emirates"> United Arab Emirates </option>
                      <option value="United Kingdom"> United Kingdom </option>
                      <option value="Uruguay"> Uruguay </option>
                      <option value="Uzbekistan"> Uzbekistan </option>
                      <option value="Vanuatu"> Vanuatu </option>
                      <option value="Vatican City"> Vatican City </option>
                      <option value="Venezuela"> Venezuela </option>
                      <option value="Vietnam"> Vietnam </option>
                      <option value="British Virgin Islands"> British Virgin Islands </option>
                      <option value="Isle of Man"> Isle of Man </option>
                      <option value="US Virgin Islands"> US Virgin Islands </option>
                      <option value="Wallis and Futuna"> Wallis and Futuna </option>
                      <option value="Western Sahara"> Western Sahara </option>
                      <option value="Yemen"> Yemen </option>
                      <option value="Zambia"> Zambia </option>
                      <option value="Zimbabwe"> Zimbabwe </option>
                      <option value="other"> Other </option>
                    </select>
                    <label class="form-sub-label" for="input_5_country" id="sublabel_5_country" style="min-height:13px"> Country </label>
                  </span>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </li>
      <li class="form-line jf-required" data-type="control_widget" id="id_18">
        <label class="form-label form-label-top form-label-auto" id="label_18" for="input_18">
          Mobile Number
          <span class="form-required">
            *
          </span>
        </label>
      </li>
      <li class="form-line jf-required" data-type="control_textarea" id="id_7">
        <label class="form-label form-label-top form-label-auto" id="label_7" for="input_7">
          Property description
          <span class="form-required">
            *
          </span>
        </label>
        <div id="cid_7" class="form-input-wide jf-required">
          <textarea id="input_7" class="form-textarea validate[required]" name="q7_propertyDescription" cols="40" rows="6" data-component="textarea" required=""></textarea>
        </div>
      </li>
      <li class="form-line jf-required" data-type="control_fileupload" id="id_10">
        <label class="form-label form-label-top form-label-auto" id="label_10" for="input_10">
          Property Photos
          <span class="form-required">
            *
          </span>
        </label>
        <div id="cid_10" class="form-input-wide jf-required">
          <div data-wrapper-react="true">
            <div data-wrapper-react="true" class="validate[multipleUpload] validate[required]"><div class="qq-uploader"><div class="qq-upload-drop-area" style="display: none;"><span>Drop files here to upload</span></div><div class="qq-upload-button form-submit-button-simple_green" style="position: relative; overflow: hidden; direction: ltr;">
                Browse Files
              </div><div class="inputContainer" style="overflow: hidden; position: absolute; top: 0px; right: 0px; width: 224px; height: 54px;"><input multiple="multiple" type="file" name="file" style="position: absolute; right: 0px; top: 0px; font-family: Arial; font-size: 118px; margin: 0px; padding: 0px; cursor: pointer; opacity: 0;"></div><div class="form-sub-label"></div><span style="display:none" class="multipleFileUploadLabels cancelText">undefined</span><span style="display:none" class="multipleFileUploadLabels ofText">undefined</span><ul class="qq-upload-list"></ul></div></div>
            <span style="display:none" class="cancelText">
              Cancel
            </span>
            <span style="display:none" class="ofText">
              of
            </span>
          </div>
        </div>
      </li>
      <li class="form-line" data-type="control_paypal" id="id_14">
        <label class="form-label form-label-top form-label-auto" id="label_14" for="input_14"> Advertising packages </label>
        <div id="cid_14" class="form-input-wide">
          <div data-wrapper-react="true">
            <input type="hidden" name="simple_fpc" data-payment_type="paypal" data-component="payment1" value="14">
            <input type="hidden" name="payment_total_checksum" id="payment_total_checksum" data-component="payment2">
            <div data-wrapper-react="true">
              <input type="hidden" id="payment_enable_lightbox">
              <br>
              <span class="form-product-item hover-product-item">
                <img src="https://www.jotform.com/uploads/Zeev123/form_files/Logo.5ad9d0cab73e69.87843015.png" alt="product" class="form-product-image-with-options" width="50" style="height:auto;vertical-align:middle">
                <div data-wrapper-react="true" class="form-product-item-detail">
                  <input type="checkbox" class="form-checkbox " id="input_14_1001" name="q14_advertisingPackages[][id]" value="1001" checked="" disabled="">
                  <label for="input_14_1001" class="form-product-container">
                    <span data-wrapper-react="true">
                      <span class="form-product-name" id="product-name-input_14_1001">
                        One Week Advertising
                      </span>
                      <span class="form-product-details">
                        <b>
                          <span data-wrapper-react="true">
                            $
                            <span id="input_14_1001_price">
                              50.00
                            </span>
                          </span>
                        </b>
                      </span>
                    </span>
                  </label>
                  <br>
                  <br>
                  <span class="form-sub-label-container" style="vertical-align:top">
                    <label class="form-sub-label" for="input_14_quantity_1001_0" style="min-height:13px"> Quantity </label>
                    <select class="form-dropdown" name="q14_advertisingPackages[special_1001][item_0]" id="input_14_quantity_1001_0">
                      <option value="0"> 0 </option>
                      <option value="1"> 1 </option>
                      <option value="2"> 2 </option>
                      <option value="3"> 3 </option>
                      <option value="4"> 4 </option>
                      <option value="5"> 5 </option>
                    </select>
                  </span>
                  <br>
                  <span class="form-special-subtotal">
                    <span class="form-item-subtotal">
                      Item subtotal:
                    </span>
                    <span data-wrapper-react="true">
                      $
                      <span id="input_14_1001_item_subtotal">
                        0.00
                      </span>
                    </span>
                  </span>
                </div>
              </span>
              <br>
              <span class="form-payment-divider" style="margin:0 0 8px">
              </span>
              <span class="form-payment-subtotal form-payment-label">
                <span id="subtotal-text">Subtotal</span>
                &nbsp;
                <span class="form-payment-price">
                  <span data-wrapper-react="true">
                    $
                    <span id="payment_subtotal">0.00</span>
                  </span>
                </span>
              </span>
              <span class="form-payment-tax form-payment-label">
                <span id="tax-text">Tax</span>
                &nbsp;
                <span class="form-payment-price">
                  <span data-wrapper-react="true">
                    $
                    <span id="payment_tax">0.00</span>
                  </span>
                </span>
              </span>
              <span class="form-payment-total">
                <b>
                  <span id="total-text">Total</span>
                  &nbsp;
                  <span class="form-payment-price">
                    <span data-wrapper-react="true">
                      $
                      <span id="payment_total">0.00</span>
                    </span>
                  </span>
                </b>
              </span>
            </div>
          </div>
        </div>
      </li>
      <li id="cid_17" class="form-input-wide" data-type="control_head">
        <div class="form-header-group ">
          <div class="header-text httal htvam">
            <h2 id="header_17" class="form-header" data-component="header">
            </h2>
            <div id="subHeader_17" class="form-subHeader">
              After clicking "Submit", you'll be redirected to PayPal.
            </div>
          </div>
        </div>
      </li>
     <input name="submit_ad" type="submit" value="Submit">
    </ul>
  </div>
  </form>
	<?php
   
	if( $_POST['submit'] ) {
		
		$name_prefix   =   sanitize_text_field( $_POST['name_prefix'] );
		$name_first   =   sanitize_text_field( $_POST['name_first'] );
		$name_last   =   sanitize_text_field( $_POST['name_last'] );
		$comp_name   =   sanitize_text_field( $_POST['comp_name'] );
		$comp_id_num   =   sanitize_user( $_POST['comp_id_num'] );
		$street_address   =   sanitize_text_field( $_POST['street_address'] );
		$street_address_2   =   sanitize_text_field( $_POST['street_address_2'] );
		$city   =   sanitize_text_field( $_POST['city'] );
		$state_province   =   sanitize_text_field( $_POST['state_province'] );
		$postal_zipcode   =   sanitize_text_field( $_POST['postal_zipcode'] );
		$country   =   sanitize_text_field( $_POST['country'] );
		$phone_number1   =   sanitize_user( $_POST['phone_number1'] );
		$phone_number2   =   sanitize_user( $_POST['phone_number2'] );
		$email   =   sanitize_email( $_POST['email'] );
		$sugession_topic   =   esc_textarea( $_POST['sugession_topic'] );
		$attach_gov_photo   =   sanitize_user( $_POST['attach_gov_photo'] );
		$role   =   'agent';
		
		
		/* $userdata = array(
        'name_prefix'    =>   $name_prefix,
        'name_first'    =>   $name_first,
        'name_last'     =>   $name_last,
        'comp_name'      =>   $comp_name,
        'comp_id_num'      =>   $comp_id_num,
        'street_address'      =>   $street_address,
        'street_address_2'    =>   $street_address_2,
        'city'      =>   $city,
        'state_province'      =>   $state_province,
        'postal_zipcode'      =>   $postal_zipcode,
        'country'      =>   $country,
        'phone_number1'      =>   $phone_number1,
        'phone_number2'      =>   $phone_number2,
        'email'      =>   $email,
        'sugession_topic'      =>   $sugession_topic,
        'attach_gov_photo'      =>   $attach_gov_photo,
        'role'      =>   $role
        
        ); */
		$website = "http://example.com";
$userdata = array(
    'user_login'  =>  $name_first,
    'user_url'    =>  $website,
    'user_email'    =>  $email,
    'display_name'    =>  $email,
    'first_name'    =>  $name_first,
    'last_name'    =>  $name_last,
    'user_email'    =>  $email,
    'user_email'    =>  $email,
	'role'      =>   'agent',
    'user_pass'   =>  'sas@31231'  // When creating an user, `user_pass` is expected.
);
        $user = wp_insert_user( $userdata );
		$user_id = 'user_'.$user;
	update_feild($user_id,'comp_name',$comp_name);
		echo $user; 	
		echo 'Registration complete. Goto <a href="' . get_site_url() . '/wp-login.php">login page</a>.'; 	
		
	}


	?> 
	  
    </div><!-- end 9col container-->
    
<?php  include(locate_template('sidebar.php')); 
wp_suspend_cache_addition(false);?>
</div>   
<?php get_footer(); ?>