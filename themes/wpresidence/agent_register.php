<?php
// Template Name: Agent Register
// Wp Estate Pack
get_header();
wp_suspend_cache_addition(true);
$options=wpestate_page_details($post->ID);
global $no_listins_per_row;
$no_listins_per_row       =   intval( get_option('wp_estate_agent_listings_per_row', '') );

$col_class=4;
if($options['content_class']=='col-md-12'){
    $col_class=3;
}

if($no_listins_per_row==3){
    $col_class  =   '6';
    $col_org    =   6;
    if($options['content_class']=='col-md-12'){
        $col_class  =   '4';
        $col_org    =   4;
    }
}else{   
    $col_class  =   '4';
    $col_org    =   4;
    if($options['content_class']=='col-md-12'){
        $col_class  =   '3';
        $col_org    =   3;
    }
}
   
	if( $_POST['submit'] ) {
		
		$name_prefix   =   sanitize_text_field( $_POST['name_prefix'] );
		$name_first   =   sanitize_text_field( $_POST['name_first'] );
		$name_last   =   sanitize_text_field( $_POST['name_last'] );
		$company_name   =   sanitize_text_field( $_POST['comp_name'] );
		$comp_id_num   =   sanitize_user( $_POST['comp_id_num'] );
		$street_address   =   sanitize_text_field( $_POST['street_address'] );
		$street_address_2   =   sanitize_text_field( $_POST['street_address_2'] );
		$city   =   sanitize_text_field( $_POST['city'] );
		$state_province   =   sanitize_text_field( $_POST['state_province'] );
		$postal_zipcode   =   sanitize_text_field( $_POST['postal_zipcode'] );
		$country   =   sanitize_text_field( $_POST['country'] );
		$phone_number1   =   sanitize_user( $_POST['phone_number1'] );
		$phone_number2   =   sanitize_user( $_POST['phone_number2'] );
		$email   =   sanitize_email( $_POST['email'] );
		$sugession_topic   =   esc_textarea( $_POST['sugession_topic'] );
		$attach_gov_photo   =   sanitize_user( $_POST['attach_gov_photo'] );		
		$password   =   sanitize_user( $_POST['password'] );		
		$website = "http://example.com";

		
$userdata = array(
    'user_login'  =>  $name_first,
    'user_url'    =>  $website,
    'user_email'    =>  $email,
    'display_name'    => $name_first,
    'first_name'    =>  $name_first,
    'last_name'    =>  $name_last,
    'user_email'    =>  $email,
	'role'      =>   'agent',
    'user_pass'   =>  $password   // When creating an user, `user_pass` is expected.
);
$user = wp_insert_user( $userdata );
$user_id = 'user_'.$user;
$field_key = 'company_name';
$value = "some new string";
update_field('company_name', $company_name, $user_id );
update_field( 'company_id',$comp_id_num , $user_id );
update_field( 'phone_number',$phone_number1 , $user_id );
update_field( 'suggestions_topics',$sugession_topic , $user_id );
update_field( 'photo_id',$attach_gov_photo , $user_id );
/* if($comp_name){
update_field('company_name',$comp_name,$user_id);
} */

}

?>

	<div class="row">
    <?php get_template_part('templates/breadcrumbs'); ?>
    <div class=" <?php print esc_html($options['content_class']);?> ">
    <?php get_template_part('templates/ajax_container'); ?>

    <form action="" method="post">
    
	<div class="row">
		<div class="col col-sm-4 form-group">
		<label for="real_stateagency_manager">Type a question <strong>*</strong></label>
			<input type="radio" class="form-control" name="real_stateagency_manager" value="">
		</div>
		
		<div class="col col-sm-4 form-group">
		<label for="real_stateagency_manager"> </label>
			<input type="radio" class="form-control" name="real_stateagency_manager" value="">
		</div>
    </div>
     
    <div class="row">
		<div class="form-group col col-sm-4">
		<label for="name_prefix">Name Prefix <strong>*</strong></label>
		<input type="text" class="form-control"name="name_prefix" value="">
		</div> 
		
		<div class="form-group col col-sm-4">
		<label for="name_first">Frist Name <strong>*</strong></label>
		<input type="text" class="form-control" name="name_first" value="">
		</div> 
		
		<div class="form-group col col-sm-4">
		<label for="name_last">Last Name <strong>*</strong></label>
		<input type="text" class="form-control" name="name_last" value="">
		</div>
    </div>
     
    <div class="form-group">
    <label for="comp_name">Company Name <strong>*</strong></label>
    <input type="text" class="form-control" name="comp_name" value="">
    </div>
     
    <div class="form-group">
    <label for="comp_id_num">COMPANY ID NUMBER</label>
    <input type="text" class="form-control" name="comp_id_num" value="">
    </div>
     
    <div class="form-group">
    <label for="street_address">Street Address</label>
    <input type="text" class="form-control" name="street_address" value="">
    
	<label for="street_address_2">Street Address line 2</label>
	<input type="text" class="form-control" name="street_address_2" value="">
    </div>
     
    <div class="form-group">
    <label for="city">City</label>
    <input type="text" class="form-control" name="city" value="">
    </div>
    
	<div class="form-group">
    <label for="state_province">State / Province</label>
    <input type="text"  class="form-control"name="state_province" value="">
    </div>
	
	<div class="form-group">
    <label for="postal_zipcode">Postal / Zip Code</label>
    <input type="text"  class="form-control"name="postal_zipcode" value="">
    </div>
	
	<div class="form-group">
    <label for="country">Country</label>
    <input type="text"  class="form-control"name="country" value="">
    </div>
    
	<div class="form-group">
    <label for="phone_number1">Phone Number</label>
    <input type="text"  class="form-control"name="phone_number1" value="">
    <input type="text"  class="form-control"name="phone_number2" value="">
    </div>
    
	<div class="form-group">
    <label for="email">E-mail</label>
    <input type="text"  class="form-control"name="email" value="">
    </div>
	   
	<div class="form-group">
    <label for="email">Password</label>
    <input type="text"  class="form-control"name="password" value="">
    </div>
	<div class="form-group">
    <label for="sugession_topic">Suggestions or topics you would like to be included in the workshop?</label>
    <input type="text"  class="form-control"name="sugession_topic" value="">
    </div>
	
	<div class="form-group">
    <label for="attach_gov_photo">Attach Government Photo ID and License Real Estate Broker</label>
    <input type="file"  class="form-control"name="attach_gov_photo" value="">
    </div>
     
    <input type="submit" class="btn btn-danger btn-large" name="submit" value="Submit Form"/>
    </form>
    

	  
    </div><!-- end 9col container-->
    
<?php  include(locate_template('sidebar.php')); 
wp_suspend_cache_addition(false);?>
</div>   
<?php get_footer(); ?>