<?php
$current_user = wp_get_current_user();
$userID                 =   $current_user->ID;
$user_login             =   $current_user->user_login;
$first_name             =   get_the_author_meta( 'first_name' , $userID );
$last_name              =   get_the_author_meta( 'last_name' , $userID );
$user_email             =   get_the_author_meta( 'user_email' , $userID );
$user_mobile            =   get_the_author_meta( 'mobile' , $userID );
$user_phone             =   get_the_author_meta( 'phone' , $userID );
$description            =   get_the_author_meta( 'description' , $userID );
$facebook               =   get_the_author_meta( 'facebook' , $userID );
$twitter                =   get_the_author_meta( 'twitter' , $userID );
$linkedin               =   get_the_author_meta( 'linkedin' , $userID );
$pinterest              =   get_the_author_meta( 'pinterest' , $userID );
$userinstagram          =   get_the_author_meta( 'instagram' , $userID );
$user_skype             =   get_the_author_meta( 'skype' , $userID );
$website                =   get_the_author_meta( 'website' , $userID );


$user_title             =   get_the_author_meta( 'title' , $userID );
$user_custom_picture    =   get_the_author_meta( 'custom_picture' , $userID );
$user_small_picture     =   get_the_author_meta( 'small_custom_picture' , $userID );
$image_id               =   get_the_author_meta( 'small_custom_picture',$userID); 
$about_me               =   get_the_author_meta( 'description' , $userID );
if($user_custom_picture==''){
    $user_custom_picture=get_template_directory_uri().'/img/default_user.png';
}
?>

<div class="col-md-12 user_profile_div"> 
    <div id="profile_message">
        </div> 
<div class="add-estate profile-page profile-onprofile row"> 
    <div class="col-md-4 profile_label">
        <div class="user_details_row"><?php _e('User Details','wpestate');?></div> 
        <div class="user_profile_explain"><?php _e('Add your contact information.','wpestate')?></div>

    </div>
          
    <div class="col-md-4">
        <p>
            <label for="firstname"><?php _e('First Name','wpestate');?></label>
            <input type="text" id="firstname" class="form-control" value="<?php echo esc_html($first_name);?>"  name="firstname">
        </p>

        <p>
            <label for="secondname"><?php _e('Last Name','wpestate');?></label>
            <input type="text" id="secondname" class="form-control" value="<?php echo esc_html($last_name);?>"  name="firstname">
        </p>
        <p>
            <label for="useremail"><?php _e('Email','wpestate');?></label>
            <input type="text" id="useremail"  class="form-control" value="<?php echo esc_html($user_email);?>"  name="useremail">
        </p>
    </div>  

    <div class="col-md-4">
        <p>
            <label for="usermobile"><?php _e('Mobile', 'wpestate'); ?></label>
            <input type="text" id="usermobile" class="form-control" value="<?php echo esc_html($user_mobile); ?>"  name="usermobile">
        </p>

        <p>
            <label for="userskype"><?php _e('WhatsApp', 'wpestate'); ?></label>
            <input type="text" id="userskype" class="form-control" value="<?php echo esc_html($user_skype); ?>"  name="userskype">
        </p>
        <?php   wp_nonce_field( 'profile_ajax_nonce', 'security-profile' );   ?>
       
    </div>

</div>

<div class="add-estate profile-page profile-onprofile row">
    <div class="col-md-4 profile_label">
	&nbsp;
    </div>
    <div class="col-md-8">
		<p class="fullp-button">
            <button class="wpresidence_button" id="update_profile"><?php _e('Update profile', 'wpestate'); ?></button>
        </p>
        
    </div>
    
            
</div>
      
<div class="add-estate profile-page profile-onprofile row"> 
    <div class="col-md-4 profile_label">
        <div class="change_pass"><?php _e('Change Password','wpestate');?></div> 
        <div class="pass_note"><?php _e('*After you change the password you will have to login again.','wpestate')?></div>

    </div>  
    <div class="col-md-8 dashboard_password">
        <p  class="col-md-12">
            <label for="oldpass"><?php _e('Old Password','wpestate');?></label>
            <input  id="oldpass" value=""  class="form-control" name="oldpass" type="password">
        </p>

        <p  class="col-md-6">
            <label for="newpass"><?php _e('New Password ','wpestate');?></label>
            <input  id="newpass" value="" class="form-control" name="newpass" type="password">
        </p>
        <p  class="col-md-6">
            <label for="renewpass"><?php _e('Confirm New Password','wpestate');?></label>
            <input id="renewpass" value=""  class="form-control" name="renewpass"type="password">
        </p>

        <?php   wp_nonce_field( 'pass_ajax_nonce', 'security-pass' );   ?>
        <p class="fullp-button">
            <button class="wpresidence_button" id="change_pass"><?php _e('Reset Password','wpestate');?></button>

        </p>
    </div>
</div> 
<?php
 if(isset($_POST['delete_user'])){
		$id= $_POST['user_id'];
		wp_delete_user( $id);
		wp_redirect( home_url() ); exit;
 }
?>

<div class="add-estate profile-page profile-onprofile row">

    <div class="col-md-4 profile_label">
        <div class="change_pass"><?php _e('Delete User','wpestate');?></div> 
    </div>  
    <div class="col-md-8 dashboard_password">
        <p class="fullp-button">
		<form name="delete_user_form" method="post" action="">
			<input type="hidden" name="user_id" value="<?php echo $userID; ?>">
            <input type="submit" name="delete_user" class="wpresidence_button" id="change_pass" Value="Delete">
		</form>
        </p>
    </div>
</div>    
</div>